# PointlessMan

The most pointless Unity3D editor plugin ever written!

![pointless image](Docs~/pointlessman.png)

# Introduction
Have you ever wanted to play Pac-Man in a Unity3D Editor window? No, me neither!
Yet I needed to write this pointless satirical plugin because the Unity3D editor scripting "framework" relies on an Update based loop. I think it is pointless for the Unity3D editor itself and it should have a nice event based system. So, enjoy playing PointlessMan straight in your editor!

# How to start?
Just go to the `HamerSoft/PointlessMan` menu in the Unity3D menu bar and select `Play`. You can also choose `Upload` for your own pointless mazes when you have some time to kill.

# Custom Mazes
You can create your own maze based on the following tile-set:

## Maze

| Image | # Id| | Image | # Id| | Image | # Id| | Image | # Id| | Image | # Id|
| :-----| :---| :---| :-----| :---|:---| :-----| :---| :---| :-----| :---|:---| :-----| :---|
|![maze -1](src/Resources/Scene/Maze/maze_-1.png)|-1| |![maze 11](src/Resources/Scene/Maze/maze_11.png)|11| |![maze 23](src/Resources/Scene/Maze/maze_23.png)|23| |![maze 35](src/Resources/Scene/Maze/maze_35.png)|35| |![maze 47](src/Resources/Scene/Maze/maze_47.png)|47|
|![maze 0](src/Resources/Scene/Maze/maze_0.png)|0| |![maze 12](src/Resources/Scene/Maze/maze_12.png)|12| |![maze 24](src/Resources/Scene/Maze/maze_24.png)|24| |![maze 36](src/Resources/Scene/Maze/maze_36.png)|36| |![maze 48](src/Resources/Scene/Maze/maze_48.png)|48|
|![maze 1](src/Resources/Scene/Maze/maze_1.png)|1| |![maze 13](src/Resources/Scene/Maze/maze_13.png)|13| |![maze 25](src/Resources/Scene/Maze/maze_25.png)|25| |![maze 37](src/Resources/Scene/Maze/maze_37.png)|37| | | |
|![maze 2](src/Resources/Scene/Maze/maze_2.png)|2| |![maze 14](src/Resources/Scene/Maze/maze_14.png)|14| |![maze 26](src/Resources/Scene/Maze/maze_26.png)|26| |![maze 38](src/Resources/Scene/Maze/maze_38.png)|38| | | |
|![maze 3](src/Resources/Scene/Maze/maze_3.png)|3| |![maze 15](src/Resources/Scene/Maze/maze_15.png)|15| |![maze 27](src/Resources/Scene/Maze/maze_27.png)|27| |![maze 39](src/Resources/Scene/Maze/maze_39.png)|39| | | |
|![maze 4](src/Resources/Scene/Maze/maze_4.png)|4| |![maze 16](src/Resources/Scene/Maze/maze_16.png)|16| |![maze 28](src/Resources/Scene/Maze/maze_28.png)|28| |![maze 40](src/Resources/Scene/Maze/maze_40.png)|40| | | |
|![maze 5](src/Resources/Scene/Maze/maze_5.png)|5| |![maze 17](src/Resources/Scene/Maze/maze_17.png)|17| |![maze 29](src/Resources/Scene/Maze/maze_29.png)|29| |![maze 41](src/Resources/Scene/Maze/maze_41.png)|41| | | |
|![maze 6](src/Resources/Scene/Maze/maze_6.png)|6| |![maze 18](src/Resources/Scene/Maze/maze_18.png)|18| |![maze 30](src/Resources/Scene/Maze/maze_30.png)|30| |![maze 42](src/Resources/Scene/Maze/maze_42.png)|42| | | |
|![maze 7](src/Resources/Scene/Maze/maze_7.png)|7| |![maze 19](src/Resources/Scene/Maze/maze_19.png)|19| |![maze 31](src/Resources/Scene/Maze/maze_31.png)|31| |![maze 43](src/Resources/Scene/Maze/maze_43.png)|43| | | |
|![maze 8](src/Resources/Scene/Maze/maze_8.png)|8| |![maze 20](src/Resources/Scene/Maze/maze_20.png)|20| |![maze 32](src/Resources/Scene/Maze/maze_32.png)|32| |![maze 44](src/Resources/Scene/Maze/maze_44.png)|44| | | |
|![maze 9](src/Resources/Scene/Maze/maze_9.png)|9| |![maze 21](src/Resources/Scene/Maze/maze_21.png)|21| |![maze 33](src/Resources/Scene/Maze/maze_33.png)|33| |![maze 45](src/Resources/Scene/Maze/maze_45.png)|45| | | |
|![maze 10](src/Resources/Scene/Maze/maze_10.png)|10| |![maze 22](src/Resources/Scene/Maze/maze_22.png)|22| |![maze 34](src/Resources/Scene/Maze/maze_34.png)|34| |![maze 46](src/Resources/Scene/Maze/maze_46.png)|46| | | |


*You are able to spawn more than 4 ghosts in your maze.

**When you use multiple player id's it will simply use the last one.

### Special ID's
| # Id | Description|
| :-----| :---|
|44| Traversable tile, that spawns a pellet|
|47| Power pellet|
|48| Traversable, Unused tile, does not spawn Pellet|
|49|Traversable tile, does not spawn Pellet|
|50|Fruit spawn position|
|100| Player|
|101|Red ghost (Blinky), direct chase to PointlessMan |
|102|Pink ghost (Pinky, tries to position himself in front of PointlessMan|
|103|Cyan ghost (Inky), tries to position himself in front of PointlessMan|
|104|Orange ghost (Clyde), Flees from PointlessMan's Position|

## Teleporting
When you want to build a teleporter into your maze, you simply need to have 2 traversable cells linear to each other at the edges of your maze.
For Example:
```
 0, 0, 0, 0, 0, 0
 0,44,44,44,44, 0
 0,44,44,44,44, 0
44,44,44,44,44,44     <--- Teleporter on this line
 0,44,44,44,44, 0
 0,44,44,44,44, 0
 0, 0, 0, 0, 0, 0

 0 = Just some random non-traversable tile
44 = Traversable tile that spawn a pellet
``` 
