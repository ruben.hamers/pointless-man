using UnityEngine;

namespace HamerSoft.PointlessMan.Core
{
    public interface IGui
    {
        Vector2 Position { get; set; }
        void OnGui();
    }
}