using UnityEditor;
using UnityEngine;

namespace HamerSoft.PointlessMan.Core
{
    public class StaticImage : IGui
    {
        public Vector2 Position
        {
            get { return new Vector2(Rect.x, Rect.y); }
            set { Rect = new Rect(value, new Vector2(Rect.width, Rect.height)); }
        }

        private Texture2D _image;
        protected Rect Rect;

        public StaticImage(string path, Rect rect)
        {
            Rect = rect;
            Position = new Vector2(rect.x, rect.y);
            _image = Resources.Load<Texture2D>(path);
        }

        public virtual void OnGui()
        {
            if (_image)
                GUI.DrawTexture(Rect, _image);
        }
    }
}