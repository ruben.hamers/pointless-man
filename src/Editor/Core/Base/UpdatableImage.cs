using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEngine;

namespace HamerSoft.PointlessMan.Core
{
    public abstract class UpdatableImage : IGui
    {
        public Vector2 Position
        {
            get { return new Vector2(Rect.x, Rect.y); }
            set { Rect = new Rect(value, new Vector2(Rect.width, Rect.height)); }
        }

        protected Texture2D[] Images;
        protected Rect Rect;
        protected float DeltaTime;
        protected int CurrentTexture;
        private DateTime _oldTime;

        public UpdatableImage(string path, Rect rect)
        {
            Rect = rect;
            Images = Resources.LoadAll<Texture2D>(path).OrderBy(i => i.name).ToArray();
            Images = Sort(Resources.LoadAll<Texture2D>(path));//.OrderBy(i => i.name).ToArray();
            _oldTime = DateTime.Now;
        }

        public UpdatableImage(string[] path, Rect rect)
        {
            Rect = rect;
            Images = path.Select(Resources.Load<Texture2D>).OrderBy(i => i.name).ToArray();
            _oldTime = DateTime.Now;
        }
        private Texture2D[] Sort(IEnumerable<Texture2D> list)
        {
            if(!list.Any())
                return new Texture2D[0];
            int maxLen = list.Select(s => s.name.Length).Max();

            return list.Select(s => new
                {
                    OrgStr = s,
                    SortStr = Regex.Replace(s.name, @"(\d+)|(\D+)", m => m.Value.PadLeft(maxLen, char.IsDigit(m.Value[0]) ? ' ' : '\xffff'))
                })
                .OrderBy(x => x.SortStr)
                .Select(x => x.OrgStr).ToArray();
        }

        public virtual void OnGui()
        {
            var now = DateTime.Now;
            DeltaTime = (float) (now - _oldTime).TotalSeconds;
            _oldTime = now;
            if (CurrentTexture >= 0 && CurrentTexture < Images.Length)
                GUI.DrawTexture(Rect, Images[CurrentTexture]);
        }
    }
}