using System;
using System.Collections.Generic;
using System.Linq;

namespace HamerSoft.PointlessMan.Core.Extensions
{
    public static class ListExtensions
    {
        public static List<T> Shuffle<T>(this List<T> self, int seed)
        {
            System.Random r = new Random(seed);
            return self.OrderBy(x => r.Next()).ToList();
        }
    }
}