using UnityEngine;

namespace HamerSoft.PointlessMan.Core.Extensions
{
    public static class Vector2Extensions
    {
        public static bool AlmostEquals(this Vector2 v1, Vector2 v2, float precision = float.Epsilon)
        {
            return v1.x.AlmostEquals(v2.x, precision) &&
                   v1.y.AlmostEquals(v2.y, precision);
        }
    }
}