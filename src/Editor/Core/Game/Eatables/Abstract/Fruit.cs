using System;
using HamerSoft.EditorAudio;
using UnityEngine;

namespace HamerSoft.PointlessMan.Core.Game
{
    public abstract class Fruit : StaticImage, IEatable
    {
        public event Action<int> Eaten;
        private EditorAudioSource _audioSource;
        public bool IsEaten { get; private set; }
        public abstract int Points { get; }

        protected Fruit(string path, Rect rect) : base(path, rect)
        {
            IsEaten = false;
        }

        public void SetRect(Rect rect)
        {
            Rect = rect;
        }

        public void SetEatSound(EditorAudioSource audiosource)
        {
            _audioSource = audiosource;
        }

        public void Eat()
        {
            Eaten?.Invoke(Points);
            _audioSource?.Play();
        }
    }
}