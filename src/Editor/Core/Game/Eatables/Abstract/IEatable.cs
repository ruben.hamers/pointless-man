using System.Collections.Generic;

namespace HamerSoft.PointlessMan.Core
{
    public interface IEatable : IGui
    {
        bool IsEaten { get; }
        void Eat();
        int Points { get; }
        void SetEatSound(EditorAudio.EditorAudioSource audiosource);
    }
}