using UnityEngine;

namespace HamerSoft.PointlessMan.Core.Game
{
    public class Apple : Fruit
    {
        public override int Points => 700;

        public Apple(string path, Rect rect) : base(path, rect)
        {
        }
    }
}