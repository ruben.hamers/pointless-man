using UnityEngine;

namespace HamerSoft.PointlessMan.Core.Game
{
    public class Bell : Fruit
    {
        public override int Points => 3000;

        public Bell(string path, Rect rect) : base(path, rect)
        {
        }
    }
}