using UnityEngine;

namespace HamerSoft.PointlessMan.Core.Game
{
    public class Cherry : Fruit
    {
        public override int Points => 100;

        public Cherry(string path, Rect rect) : base(path, rect)
        {
        }
    }
}