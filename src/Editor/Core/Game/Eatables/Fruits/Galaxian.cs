using UnityEngine;

namespace HamerSoft.PointlessMan.Core.Game
{
    public class Galaxian : Fruit
    {
        public override int Points => 2000;

        public Galaxian(string path, Rect rect) : base(path, rect)
        {
        }
    }
}