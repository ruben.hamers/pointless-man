using UnityEngine;

namespace HamerSoft.PointlessMan.Core.Game
{
    public class Key : Fruit
    {
        public override int Points => 5000;

        public Key(string path, Rect rect) : base(path, rect)
        {
        }
    }
}