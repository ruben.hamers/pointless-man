using UnityEngine;

namespace HamerSoft.PointlessMan.Core.Game
{
    public class Melon : Fruit
    {
        public override int Points => 1000;

        public Melon(string path, Rect rect) : base(path, rect)
        {
        }
    }
}