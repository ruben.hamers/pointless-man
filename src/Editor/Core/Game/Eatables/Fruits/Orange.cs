using UnityEngine;

namespace HamerSoft.PointlessMan.Core.Game
{
    public class Orange : Fruit
    {
        public override int Points => 500;

        public Orange(string path, Rect rect) : base(path, rect)
        {
        }
    }
}