using UnityEngine;

namespace HamerSoft.PointlessMan.Core.Game
{
    public class Strawberry : Fruit
    {
        public override int Points => 300;

        public Strawberry(string path, Rect rect) : base(path, rect)
        {
        }
    }
}