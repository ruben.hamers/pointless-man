using HamerSoft.EditorAudio;
using UnityEngine;

namespace HamerSoft.PointlessMan.Core.Game
{
    public class Pellet : StaticImage, IEatable
    {
        private EditorAudioSource _audioSource;
        public bool IsEaten { get; private set; }
        public int Points => 10;

        public Pellet(string path, Rect rect) : base(path, rect)
        {
            IsEaten = false;
        }

        public void Eat()
        {
            if (IsEaten)
                return;
            if (!_audioSource.IsPlaying)
                _audioSource.Play();
            IsEaten = true;
        }

        public void SetEatSound(EditorAudioSource audiosource)
        {
            _audioSource = audiosource;
        }

        public override void OnGui()
        {
            if (!IsEaten)
                base.OnGui();
        }
    }
}