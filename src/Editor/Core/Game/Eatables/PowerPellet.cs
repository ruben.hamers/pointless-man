using System;
using HamerSoft.EditorAudio;
using UnityEngine;

namespace HamerSoft.PointlessMan.Core.Game
{
    public class PowerPellet : UpdatableImage, IEatable
    {
        public event Action<PowerPellet> Eaten;
        public bool IsEaten { get; private set; }
        private float _interval, _time;
        private EditorAudioSource _audioSource;
        public int Points => 50;

        public PowerPellet(string[] path, Rect rect) : base(path, rect)
        {
            IsEaten = false;
            CurrentTexture = 1;
            _interval = .2f;
        }

        public void Eat()
        {
            if (IsEaten)
                return;
            _audioSource?.Play();
            IsEaten = true;
            CurrentTexture = 0;
            OnEaten();
        }

        private void OnEaten()
        {
            Eaten?.Invoke(this);
        }

        public void SetEatSound(EditorAudioSource audiosource)
        {
            _audioSource = audiosource;
        }

        public override void OnGui()
        {
            base.OnGui();
            if (!IsEaten)
            {
                _time += DeltaTime;
                if (_time > _interval)
                {
                    if (CurrentTexture == Images.Length - 1)
                        CurrentTexture = 0;
                    else
                        CurrentTexture++;
                    _time = 0;
                }
            }
        }
    }
}