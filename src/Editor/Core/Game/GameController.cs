using System;
using System.Collections.Generic;
using System.Linq;
using System.Timers;
using HamerSoft.EditorAudio;
using HamerSoft.PointlessMan.Core.Game;
using HamerSoft.PointlessMan.Core.Game.Player;
using Microsoft.Win32;
using PointlessMan.Core.Game.PathfindingAlgorithms;
using UnityEngine;

namespace HamerSoft.PointlessMan.Core
{
    public class GameController : IGui, IPausable, IDisposable
    {
        /// <summary>
        /// 10000 points grant an extra life
        /// </summary>
        private const int FirstLiveThreshold = 10000;

        /// <summary>
        /// 100000 grand the second extra life
        /// </summary>
        private const int SecondLivesThreshold = 100000;

        /// <summary>
        /// first 64 grant a fruit
        /// </summary>
        private const int FIRST_FRUIT_TRESHOLD = 64;

        /// <summary>
        /// when there are only 66 pellets left to be eaten
        /// </summary>
        private const int SECOND_FRUIT_TRESHOLD = 66;

        public event Action GameOver, GameWon, PlayerDied;
        private bool _paused, _disposed, _started;
        public Vector2 Position { get; set; }
        protected Maze Maze;
        protected Player Player;
        protected List<Ghost> Ghosts;
        private int _lives, s, _currentLevel, _livesThreshold, _ghostsEatenInSuccession, _pelletsEatenInSuccession;
        private Dictionary<int, Fruit> _fruits;
        private float _ghostTeleportDuration, _ghostTeleportSpeed;
        private static DateTime _oldTime;
        private EditorAudioSource _extraLifeSource;
        private Hud _hud;

        private int _score
        {
            get { return s; }
            set
            {
                s = value;
                _hud.SetScore(s);
                if (_livesThreshold <= SecondLivesThreshold && s % _livesThreshold == 0)
                {
                    _lives++;
                    _hud.UpdateLives(_lives);
                    _extraLifeSource.Play();
                    _livesThreshold = SecondLivesThreshold;
                }
            }
        }

        public GameController(Maze maze)
        {
            Maze = maze;
            _hud = new Hud(maze.Rect, maze.Scale);
            Maze.Position = new Vector2(0,120);
            _paused = true;
            _started = false;
            _ghostTeleportDuration = 5;
            _ghostTeleportSpeed = .25f;
            _ghostsEatenInSuccession = _pelletsEatenInSuccession = 0;
            _livesThreshold = FirstLiveThreshold;
            Maze.PowerPelletEaten += MazeOnPowerPelletEaten_Handler;
            maze.PelletEaten += MazeOnPelletEaten;
            CreatePlayer(maze);
            CreateGhosts(maze);
            _lives = 3;
            _hud.UpdateLives(_lives);
            _extraLifeSource = new EditorAudioSource(Resources.Load<AudioClip>("Audio/pacman_extrapac"));
            _fruits = new Dictionary<int, Fruit>
            {
                {1, new Cherry("Scene/Score/Cherry", new Rect())},
                {2, new Strawberry("Scene/Score/StrawBerry", new Rect())},
                {3, new Orange("Scene/Score/Orange", new Rect())},
                {5, new Apple("Scene/Score/Apple", new Rect())},
                {7, new Melon("Scene/Score/Melon", new Rect())},
                {9, new Galaxian("Scene/Score/Galaxian", new Rect())},
                {11, new Bell("Scene/Score/Bell", new Rect())},
                {13, new Key("Scene/Score/Key", new Rect())},
            };
        }

        public void Start()
        {
            Pause();
            _started = false;
            var startAudio = new EditorAudioSource(Resources.Load<AudioClip>("Audio/pacman_beginning"));
            Timer t = new Timer(startAudio.GetDuration());

            void Elapsed(object target, ElapsedEventArgs e)
            {
                var timer = target as Timer;
                timer.Elapsed -= Elapsed;
                timer.Dispose();
                t = null;
                Pause(false);
                _started = true;
            }

            t.Elapsed += Elapsed;
            t.Start();
            startAudio.Play();
        }

        public void SetLives(int lives = 3)
        {
            _lives = lives;
            _hud.UpdateLives(_lives);
        }

        public void SetGhostTeleportSpeed(float speed = .25f, float duration = 5)
        {
            _ghostTeleportSpeed = speed;
            _ghostTeleportDuration = duration;
        }

        private void MazeOnPowerPelletEaten_Handler(int points)
        {
            _ghostsEatenInSuccession = 0;
            Ghosts.ForEach(g => g.ActScared(5f));
        }

        private void MazeOnPelletEaten(int points)
        {
            _score += points;
            _pelletsEatenInSuccession++;
            if (_currentLevel > 0 && (_pelletsEatenInSuccession == FIRST_FRUIT_TRESHOLD ||
                                      Maze.TotalPellets - Maze.PelletsEaten == SECOND_FRUIT_TRESHOLD))
                SpawnFruit();
        }

        private void SpawnFruit()
        {
            Fruit selectedFruit = null;
            if (_currentLevel > _fruits.Last().Key)
                selectedFruit = _fruits.Last().Value;
            else
                for (int i = 0; i < _fruits.Count; i++)
                {
                    if (_fruits.ElementAt(i).Key <= _currentLevel)
                        selectedFruit = _fruits.ElementAt(i).Value;
                    else
                        break;
                }

            if (selectedFruit != null)
                Maze.SetFruit(selectedFruit);
        }

        private void CreateGhosts(Maze maze)
        {
            Rect? ghostRect;
            Ghosts = new List<Ghost>();
            int index = 1;
            var pathfinders = new AbstractPathFinder[]
            {
                new Chasing(),
                new Blocking(DateTime.MaxValue.Year),
                new Blocking(DateTime.MinValue.Year),
                new Fleeing(DateTime.MaxValue.Year - DateTime.MinValue.Year)
            };
            while ((ghostRect = maze.ReplaceInitialGhostPosition(44, out var initialPos)) != null)
            {
                var ghost = new Ghost($"Scene/Ghosts/{index}/", ghostRect.Value, maze, initialPos);
                ghost.Moved += GhostOnMoved_Handler;
                ghost.Teleported += GhostOnTeleported_Handler;
                ghost.SetEatenSprites($"Scene/Ghosts/Eaten/");
                ghost.SetScaredSprites($"Scene/Ghosts/Scared/");
                ghost.SetEatSound(new EditorAudioSource(Resources.Load<AudioClip>("Audio/pacman_eatghost")));
                ghost.SetPathfindingStrategy(pathfinders[index - 1]);
                Ghosts.Add(ghost);
                index++;
                if (index == 5)
                    index = 1;
            }
        }

        private void GhostOnTeleported_Handler(IMovable movable)
        {
            var ghost = movable as Ghost;
            ghost.StartTeleportSlowdown(_ghostTeleportSpeed, _ghostTeleportDuration);
        }

        private void CreatePlayer(Maze maze)
        {
            Player = new Player("Scene/Player/", new Rect(), maze);
            Player.Moved += PlayerOnMoved_Handler;
            Player.SetDeathSound(Resources.Load<AudioClip>("Audio/pacman_death"));
        }

        public void OnGui()
        {
            _hud?.OnGui();
            Maze?.OnGui();
            Ghosts?.ForEach(g => g.OnGui());
            Player?.OnGui();
        }

        private void RestartCurrentLevel()
        {
            Pause();
            _pelletsEatenInSuccession = _ghostsEatenInSuccession = 0;
            Player.Reset();
            Ghosts.ForEach(g => g.Reset());
            Start();
        }

        private void NextLevel()
        {
            Pause();
            _currentLevel++;
            _pelletsEatenInSuccession = _ghostsEatenInSuccession = 0;
            Player.Reset();
            Ghosts.ForEach(g => g.Reset());
            Maze.RespawnPellets();
            Start();
        }

        public void Pause(bool pause = true)
        {
            _paused = pause;
            Ghosts.ForEach(g => g.Pause(_paused));
            Player.Pause(_paused);
        }

        private void CheckIfGameLost()
        {
            if (Ghosts == null || Ghosts.Count == 0 ||
                !Ghosts.Any(g => g.CurrentPosition == Player.CurrentPosition && (!g.IsScared && !g.IsEaten)))
                return;
            Pause();
            _lives--;
            _hud.UpdateLives(_lives);
            Player.Died += PlayerOnDiedHandler;
            Player.Die();
        }

        private void PlayerOnDiedHandler()
        {
            Player.Died -= PlayerOnDiedHandler;
            if (_lives > 1)
            {
                RestartCurrentLevel();
                PlayerDied?.Invoke();
            }
            else
                OnGameOver();
        }

        private void CheckIfGameWon()
        {
            if (!Maze.Won())
                return;
            Pause();
            if (_currentLevel < 255)
                NextLevel();
            else
                OnGameWon();
        }

        private void OnGameOver()
        {
            PlayerDied?.Invoke();
            GameOver?.Invoke();
        }

        private void OnGameWon()
        {
            GameWon?.Invoke();
        }

        private void GhostOnMoved_Handler(IMovable movable)
        {
            var ghost = movable as Ghost;
            EatGhostForPoints(ghost);
            CheckIfGameLost();
        }

        private void PlayerOnMoved_Handler(IMovable movable)
        {
            Ghosts.ForEach(EatGhostForPoints);
            CheckIfGameWon();
            CheckIfGameLost();
        }

        private void EatGhostForPoints(Ghost ghost)
        {
            if (!ghost.IsScared || !ghost.CurrentPosition.Equals(Player.CurrentPosition))
                return;

            if (!ghost.IsEaten)
            {
                _score += ghost.Points * (int) Math.Pow(2, _ghostsEatenInSuccession);
                _ghostsEatenInSuccession++;
            }

            ghost.Eat();
        }

        public void Dispose()
        {
            if (_disposed)
                return;

            Maze.PelletEaten -= MazeOnPelletEaten;
            Maze.PowerPelletEaten -= MazeOnPowerPelletEaten_Handler;
            _disposed = true;
            Player.Moved -= PlayerOnMoved_Handler;
            Player.Died -= PlayerOnDiedHandler;
            foreach (var ghost in Ghosts)
            {
                ghost.Moved -= GhostOnMoved_Handler;
                ghost.Teleported -= GhostOnTeleported_Handler;
            }

            Player = null;
            Ghosts = null;
            Maze = null;
        }

        public Vector2 GetMinWindowSize()
        {
            return _hud.Rect.max;
        }
    }
}