using System;
using System.Collections.Generic;
using System.Linq;
using HamerSoft.EditorAudio;
using HamerSoft.PointlessMan.Core.Extensions;
using HamerSoft.PointlessMan.Core.Game.Pathfinding;
using PointlessMan.Core.Game.PathfindingAlgorithms;
using UnityEngine;

namespace HamerSoft.PointlessMan.Core.Game
{
    public class Ghost : MovableImage, IMovable, IPausable, IDisposable, IEatable
    {
        public event Action<Ghost> DestinationReached;
        public event Action<IMovable> Moved, Teleported;
        public Vector2Int CurrentPosition => new Vector2Int(_x, _y);
        private readonly Maze _maze;
        public int Points => 200;
        public bool IsEaten { get; private set; }
        public bool IsScared { get; private set; }
        private int _x, _y;
        private float _interval, _time, _originalSpeed, _speed, _scaredDuration, _teleportDuration;
        private Stack<Vector2Int> _path;
        private bool _paused, _teleported;
        private IPathfinder _pathfinder, _scared, _activePathfinder;
        private Vector2 _targetPosition;
        private readonly Vector2Int _startPosition;
        private Vector2Int _currentMoveDirection;
        private Texture2D[] _scaredSprites, _eatenSprites, _originalSprites;
        private EditorAudioSource _eatenAudioSource;

        public Ghost(string path, Rect rect, Maze maze, Vector2Int gridPos) : base(path, rect)
        {
            IsEaten = false;
            _maze = maze;
            _x = gridPos.x;
            _y = gridPos.y;
            _startPosition = gridPos;
            _interval = 2;
            _originalSpeed = _speed = .5f;
            _time = float.MaxValue;
            _scared = new Scared(_x * _y);
            _activePathfinder = _pathfinder = new Chasing();
            _path = _pathfinder.FindPath(maze, CurrentPosition, maze.CurrentPlayerPos);
            _originalSprites = Images;
            SetTargetPosition();
        }

        private void SetTargetPosition()
        {
            if (_path != null && _path.Count > 0)
                _targetPosition = _maze.GetCellPos(_path.Peek().x, _path.Peek().y);
        }

        public void SetScaredSprites(string path)
        {
            _scaredSprites = Resources.LoadAll<Texture2D>(path).ToArray();
        }

        public void SetEatenSprites(string path)
        {
            _eatenSprites = Resources.LoadAll<Texture2D>(path).ToArray();
        }

        public void SetPathfindingInterval(float seconds = 2)
        {
            _interval = seconds;
        }

        public void ActScared(float scaredDuration)
        {
            _paused = true;
            _scaredDuration = scaredDuration;
            _activePathfinder = _scared;
            _path = _activePathfinder.FindPath(_maze, CurrentPosition, _maze.CurrentPlayerPos);
            _time = 0;
            Images = _scaredSprites;
            IsScared = true;
            _paused = false;
        }

        private void StopActingScared()
        {
            _paused = true;
            IsScared = false;
            Images = _originalSprites;
            _activePathfinder = _pathfinder;
            _path = _activePathfinder.FindPath(_maze, CurrentPosition, _maze.CurrentPlayerPos);
            _time = 0;
            _paused = false;
        }

        public void SetMoveSpeed(float speed = .5f)
        {
            _originalSpeed = _speed = speed;
        }

        public void StartTeleportSlowdown(float speed, float duration)
        {
            _speed = speed;
            _teleportDuration = duration;
        }

        public void SetPathfindingStrategy(IPathfinder pathfinder)
        {
            _pathfinder = pathfinder;
        }

        private void SwapSprite()
        {
            if (_path.Count == 0)
                return;
            var target = _path.Peek();
            if (CurrentPosition.x < target.x && CurrentPosition.y == target.y)
            {
                _currentMoveDirection = new Vector2Int(1, 0);
                MoveRight();
            }
            else if (CurrentPosition.x > target.x && CurrentPosition.y == target.y)
            {
                _currentMoveDirection = new Vector2Int(-1, 0);
                MoveLeft();
            }
            else if (CurrentPosition.y < target.y && CurrentPosition.x == target.x)
            {
                _currentMoveDirection = new Vector2Int(0, 1);
                MoveDown();
            }
            else if (CurrentPosition.y > target.y && CurrentPosition.x == target.x)
            {
                _currentMoveDirection = new Vector2Int(0, -1);
                MoveUp();
            }
        }

        private void Move()
        {
            if (_path == null || _path.Count == 0)
                return;
            Position = Vector2.MoveTowards(Position, _targetPosition, _speed);
            if (Position.AlmostEquals(_targetPosition, .1f))
            {
                var pos = _path.Count > 0 ? _path.Pop() : CurrentPosition;
                _x = pos.x;
                _y = pos.y;
                if (_maze.IsCellTeleporter(_x, _y, _currentMoveDirection, out var teleportPos))
                {
                    Position = _maze.GetCellPos(teleportPos.x, teleportPos.y);
                    _x = teleportPos.x;
                    _y = teleportPos.y;
                    pos = _path.Count > 0 ? _path.Pop() : teleportPos;
                    OnTeleported();
                }

                SwapSprite();
                SetTargetPosition();
                OnMoved();
            }


            if (_path.Count == 0)
                DestinationReached?.Invoke(this);
        }

        public override void OnGui()
        {
            base.OnGui();
            if (!_paused)
            {
                _time += DeltaTime;
                if (IsScared && (_scaredDuration -= DeltaTime) <= 0)
                    StopActingScared();

                if (_teleported && (_teleportDuration -= DeltaTime) <= 0)
                    StopTeleportedSlowdown();

                if (!IsEaten && _time > _interval || (_path != null && _path.Count == 0))
                {
                    _path = _activePathfinder.FindPath(_maze, _path.Count > 0 ? _path.Peek() : CurrentPosition,
                        _maze.CurrentPlayerPos) ?? _path;
                    _time = 0;
                }
                else
                    Move();
            }
        }

        private void StopTeleportedSlowdown()
        {
            _teleported = false;
            _speed = _originalSpeed;
        }

        private void OnMoved()
        {
            Moved?.Invoke(this);
        }

        private void OnTeleported()
        {
            _teleported = true;
            Teleported?.Invoke(this);
        }

        public void Pause(bool pause = true)
        {
            _paused = pause;
        }

        public void Dispose()
        {
        }

        public void Reset()
        {
            if (IsEaten)
                _speed /= 3;
            DestinationReached -= StopEatenEffect;
            _scaredDuration = -1;
            IsEaten = IsScared = false;
            _x = _startPosition.x;
            _y = _startPosition.y;
            Position = _maze.GetCellPos(_x, _y);
            _path = new Stack<Vector2Int>();
            _activePathfinder = _pathfinder;
            _path = _activePathfinder.FindPath(_maze, CurrentPosition, _startPosition);
            Images = _originalSprites;
            _time = CurrentTexture = 0;
        }

        public void Eat()
        {
            if (!IsScared)
                return;
            IsEaten = true;
            IsScared = false;
            Images = _eatenSprites;
            _speed *= 3;
            _eatenAudioSource?.Play();
            CurrentTexture = 0;
            _activePathfinder = _pathfinder;
            DestinationReached += StopEatenEffect;
            _path = _activePathfinder.FindPath(_maze, CurrentPosition, _startPosition);
        }

        private void StopEatenEffect(Ghost ghost)
        {
            DestinationReached -= StopEatenEffect;
            _eatenAudioSource?.Stop();
            IsEaten = false;
            _speed /= 3;
            Images = _originalSprites;
            CurrentTexture = 0;
        }

        public void SetEatSound(EditorAudioSource audiosource)
        {
            _eatenAudioSource = audiosource;
        }
    }
}