using System.Collections.Generic;
using UnityEngine;

namespace HamerSoft.PointlessMan.Core.Game.Pathfinding
{
    //Blinky (red), Pinky (pink), Inky (cyan), and Clyde (orange) 
    //Blinky gives direct chase to Pac-Man, Pinky and Inky try to position themselves in front of Pac-Man,
    //usually by cornering him, and Clyde will switch between chasing Pac-Man and fleeing from him.
    public interface IPathfinder
    {
        Stack<Vector2Int> FindPath(Maze maze, Vector2Int self, Vector2Int target);
    }
}