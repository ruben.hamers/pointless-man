using System.Collections.Generic;
using HamerSoft.PointlessMan.Core.Game;
using HamerSoft.PointlessMan.Core.Game.Pathfinding;
using UnityEngine;

namespace PointlessMan.Core.Game.PathfindingAlgorithms
{
    public class AbstractPathFinder : IPathfinder
    {
        protected Maze Maze;

        public virtual Stack<Vector2Int> FindPath(Maze maze, Vector2Int self, Vector2Int target)
        {
            Dictionary<Vector2Int, Vector2Int?> cameFromTable = new Dictionary<Vector2Int, Vector2Int?>();
            Maze = maze;
            var grid = Maze.GetStringRepresentation();
            cameFromTable.Add(self, null);
            Queue<Vector2Int> frontier = new Queue<Vector2Int>();
            frontier.Enqueue(self);
            while (frontier.Count > 0)
            {
                Vector2Int current = frontier.Dequeue();
                foreach (var neighbour in GetNeighbours(current, grid, cameFromTable))
                {
                    frontier.Enqueue(neighbour);
                    cameFromTable[neighbour] = current;
                    if (neighbour == target)
                        break;
                }
            }

            if (cameFromTable.ContainsKey(target))
                return GetPath(target, cameFromTable);
            return null;
        }

        private Stack<Vector2Int> GetPath(Vector2Int target, Dictionary<Vector2Int, Vector2Int?> cameFromTable)
        {
            Stack<Vector2Int> path = new Stack<Vector2Int>();
            path.Push(target);
            while (cameFromTable.TryGetValue(target, out var val))
            {
                if (val == null)
                    break;
                target = val.Value;
                path.Push(target);
            }

            return path;
        }

        protected virtual List<Vector2Int> GetNeighbours(Vector2Int current, string[][] grid,
            Dictionary<Vector2Int, Vector2Int?> cameFromTable)
        {
            int x = current.x;
            int y = current.y;
            List<Vector2Int> neighbours = new List<Vector2Int>();
            Vector2Int targetPos;
            //left
            if (CanGoLeft(grid, x, y, out targetPos) &&
                !cameFromTable.ContainsKey(targetPos))
                neighbours.Add(targetPos);

            //right
            if (CanGoRight(grid, x, y, out targetPos) &&
                !cameFromTable.ContainsKey(targetPos))
                neighbours.Add(targetPos);

            //up
            if (CanGoUp(grid, x, y, out targetPos) &&
                !cameFromTable.ContainsKey(targetPos))
                neighbours.Add(targetPos);

            //down
            if (CanGoDown(grid, x, y, out targetPos) &&
                !cameFromTable.ContainsKey(targetPos))
                neighbours.Add(targetPos);
            return neighbours;
        }

        protected virtual bool CanGoDown(string[][] grid, int x, int y, out Vector2Int targetPos)
        {
            if (Maze.IsCellTeleporter(x, y, new Vector2Int(0, 1), out var teleportPos))
            {
                targetPos = teleportPos;
                return true;
            }
            else
            {
                targetPos = new Vector2Int(x, y + 1);
                return y < grid.Length - 1 && grid[y + 1][x] == "1";
            }
        }

        protected virtual bool CanGoUp(string[][] grid, int x, int y, out Vector2Int targetPos)
        {
            if (Maze.IsCellTeleporter(x, y, new Vector2Int(0, -1), out var teleportPos))
            {
                targetPos = teleportPos;
                return true;
            }
            else
            {
                targetPos = new Vector2Int(x, y - 1);
                return y > 0 && grid[y - 1][x] == "1";
            }
        }

        protected virtual bool CanGoRight(string[][] grid, int x, int y, out Vector2Int targetPos)
        {
            if (Maze.IsCellTeleporter(x, y, new Vector2Int(1, 0), out var teleportPos))
            {
                targetPos = teleportPos;
                return true;
            }
            else
            {
                targetPos = new Vector2Int(x + 1, y);
                return x < grid[y].Length - 1 && grid[y][x + 1] == "1";
            }
        }

        protected virtual bool CanGoLeft(string[][] grid, int x, int y, out Vector2Int targetPos)
        {
            if (Maze.IsCellTeleporter(x, y, new Vector2Int(-1, 0), out var teleportPos))
            {
                targetPos = teleportPos;
                return true;
            }
            else
            {
                targetPos = new Vector2Int(x - 1, y);
                return x > 0 && grid[y][x - 1] == "1";
            }
        }
    }
}