using System.Collections.Generic;
using System.Linq;
using HamerSoft.PointlessMan.Core.Extensions;
using HamerSoft.PointlessMan.Core.Game;
using UnityEngine;

namespace PointlessMan.Core.Game.PathfindingAlgorithms
{
    public class Blocking : AbstractPathFinder
    {
        private int _offset;

        public Blocking(int offset)
        {
            _offset = offset;
        }

        public override Stack<Vector2Int> FindPath(Maze maze, Vector2Int self, Vector2Int target)
        {
            Maze = maze;
            Dictionary<Vector2Int, Vector2Int?> cameFromTable = new Dictionary<Vector2Int, Vector2Int?>();
            var grid = maze.GetStringRepresentation();
            cameFromTable.Add(target, null);
            List<List<Vector2Int>> frontier = new List<List<Vector2Int>>();
            frontier.Add(new List<Vector2Int> {target});
            for (int i = 0; i < _offset; i++)
            {
                frontier.Add(new List<Vector2Int>());
                foreach (var current in frontier[i])
                foreach (var neighbour in GetNeighbours(current, grid, cameFromTable))
                {
                    frontier[i + 1].Add(neighbour);
                    cameFromTable[neighbour] = current;
                }
            }

            return base.FindPath(maze, self,
                frontier.Last().Count > 0 ? frontier.Last().Shuffle(self.x * self.y).Last() : target);
        }
    }
}