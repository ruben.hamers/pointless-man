using System.Collections.Generic;
using System.Linq;
using HamerSoft.PointlessMan.Core.Game;
using UnityEngine;
using Random = System.Random;

namespace PointlessMan.Core.Game.PathfindingAlgorithms
{
    public class Fleeing : AbstractPathFinder
    {
        private Random _random;
        private Vector2Int _playerPosition;

        public Fleeing(int randomSeed)
        {
            _random = new Random(randomSeed);
        }

        public override Stack<Vector2Int> FindPath(Maze maze, Vector2Int self, Vector2Int target)
        {
            var fleePosition = maze.GetRandomTraversableCell(_random.Next(0, 1000)).GridPosition;
            return base.FindPath(maze, self, fleePosition);
        }

        protected override bool CanGoDown(string[][] grid, int x, int y, out Vector2Int targetPos)
        {
            return base.CanGoDown(grid, x, y, out targetPos) && _playerPosition != new Vector2Int(x, y);
        }

        protected override bool CanGoLeft(string[][] grid, int x, int y, out Vector2Int targetPos)
        {
            return base.CanGoLeft(grid, x, y, out targetPos) && _playerPosition != new Vector2Int(x, y);
        }

        protected override bool CanGoRight(string[][] grid, int x, int y, out Vector2Int targetPos)
        {
            return base.CanGoRight(grid, x, y, out targetPos) && _playerPosition != new Vector2Int(x, y);
        }

        protected override bool CanGoUp(string[][] grid, int x, int y, out Vector2Int targetPos)
        {
            return base.CanGoUp(grid, x, y, out targetPos) && _playerPosition != new Vector2Int(x, y);
        }
    }
}