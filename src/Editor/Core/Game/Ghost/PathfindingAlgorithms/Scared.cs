using System.Collections.Generic;
using HamerSoft.PointlessMan.Core.Game;
using UnityEngine;
using Random = System.Random;

namespace PointlessMan.Core.Game.PathfindingAlgorithms
{
    public class Scared : AbstractPathFinder
    {
        private Random _random;

        public Scared(int randomSeed)
        {
            _random = new Random(randomSeed);
        }

        public override Stack<Vector2Int> FindPath(Maze maze, Vector2Int self, Vector2Int target)
        {
            var fleePosition = maze.GetRandomTraversableCell(_random.Next(0, 1000)).GridPosition;
            return base.FindPath(maze, self, fleePosition);
        }
    }
}