using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace HamerSoft.PointlessMan.Core
{
    public class Hud : IGui
    {
        private readonly Rect _mazeRect;
        private readonly float _scale;
        private const int CELL_SIZE = 8;

        public Rect Rect { private set; get; }

        public Vector2 Position
        {
            get { return new Vector2(Rect.x, Rect.y); }
            set { Rect = new Rect(value, new Vector2(Rect.width, Rect.height)); }
        }

        private StaticImage _background;
        private List<StaticImage> _lives;
        private string _score;
        private int _offset = 20;
        private int _spacing = 5;
        private Rect _textRect, _labelRect;
        private Font _font;
        private GUIStyle _labelStyle, _scoreStyle;

        public Hud(Rect mazeRect, float scale)
        {
            _mazeRect = mazeRect;
            _font = Resources.Load<Font>("Font/PacMan");
            _scale = scale;
            _lives = new List<StaticImage>();
            Rect = new Rect(0, 0, mazeRect.width, mazeRect.height + 120);
            _background = new StaticImage("Scene/Background", Rect);
            _labelRect = new Rect(0, 30, mazeRect.width, 30);
            _textRect = new Rect(0, 90, mazeRect.width, 30);
            _labelStyle = GetTextGuiStyle(30);
            _scoreStyle = GetTextGuiStyle(15);
        }

        private GUIStyle GetTextGuiStyle(int fontsize)
        {
            return new GUIStyle()
            {
                alignment = TextAnchor.MiddleCenter, font = _font, fontSize = fontsize,
                normal = new GUIStyleState() {textColor = Color.white},
                active = new GUIStyleState() {textColor = Color.white},
                focused = new GUIStyleState() {textColor = Color.white},
                hover = new GUIStyleState() {textColor = Color.white},
            };
        }

        public void OnGui()
        {
            _background?.OnGui();
            _lives?.ForEach(l => l.OnGui());
            EditorGUI.TextField(_labelRect, "PointlessMan!\r\nHighscore", _labelStyle);
            EditorGUI.TextField(_textRect, _score, _scoreStyle);
        }

        public void SetScore(int score)
        {
            _score = score.ToString();
        }

        public void UpdateLives(int lives)
        {
            if (lives == _lives.Count)
                return;
            _lives = new List<StaticImage>();
            for (int i = 0; i < lives; i++)
                _lives.Add(new StaticImage("Scene/Player/player_1",
                    new Rect(CELL_SIZE + i * CELL_SIZE * _scale + (_spacing * i),
                        90,
                        CELL_SIZE * _scale,
                        CELL_SIZE * _scale)));
        }
    }
}