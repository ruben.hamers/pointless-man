using System;
using UnityEngine;

namespace HamerSoft.PointlessMan.Core
{
    public interface IMovable
    {
        event Action<IMovable> Moved, Teleported;
        Vector2Int CurrentPosition { get; }
        void Reset();
    }
}