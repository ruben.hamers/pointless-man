namespace HamerSoft.PointlessMan.Core
{
    public interface IPausable
    {
        void Pause(bool pause = true);
    }
}