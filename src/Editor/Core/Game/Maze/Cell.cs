using HamerSoft.EditorAudio;
using UnityEngine;

namespace HamerSoft.PointlessMan.Core.Game
{
    public class Cell : StaticImage, IEatable
    {
        public int Number { get; }
        public bool IsTraversable { get; }
        private IEatable _pellet;
        public bool IsEaten => _pellet == null || !IsTraversable;
        public int Points => _pellet?.Points ?? 0;
        public bool IsFruitSpawn { get; private set; }
        public Vector2Int GridPosition { get; private set; }

        public Cell(int number, bool isTraversable, string path, Rect rect) : base(path, rect)
        {
            Number = number;
            IsTraversable = isTraversable;
        }

        public void SetGridPosition(int x, int y)
        {
            GridPosition = new Vector2Int(x, y);
        }

        public void SetPellet(IEatable pellet)
        {
            _pellet = pellet;
        }

        public void Eat()
        {
            if (IsEaten)
                return;
            _pellet.Eat();
            _pellet = null;
        }

        public void SetEatSound(EditorAudioSource audiosource)
        {
            _pellet?.SetEatSound(audiosource);
        }

        public Rect GetRect() => Rect;


        public void MarkAsFruitSpawn()
        {
            IsFruitSpawn = true;
        }

        public override void OnGui()
        {
            base.OnGui();
            _pellet?.OnGui();
        }

        public void AddToPosition(Vector2 newPosition)
        {
            Position += newPosition;
            if (_pellet != null)
                _pellet.Position += newPosition;
        }
    }
}