using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using HamerSoft.EditorAudio;
using UnityEngine;

namespace HamerSoft.PointlessMan.Core.Game
{
    public class Maze : IGui, IDisposable
    {
        private const int CELL_SIZE = 8;

        public event Action<int> PelletEaten, PowerPelletEaten;

        public Vector2 Position
        {
            get => Rect.position;
            set
            {
                Vector2 oldPos = Rect.position;
                Rect = new Rect(value, new Vector2(Rect.width, Rect.height));
                foreach (List<Cell> cells in _maze)
                foreach (Cell cell in cells)
                    cell.AddToPosition(value - oldPos);
            }
        }

        public int TotalPellets { get; private set; }
        public int PelletsEaten { get; private set; }
        public Vector2Int CurrentPlayerPos { get; private set; }
        public Rect Rect { get; protected set; }
        public float Scale { get; private set; }

        private readonly List<List<Cell>> _maze;
        private string[][] _stringRepresentation;
        private readonly Queue<Vector2Int> _initialGhostPositions;
        private List<PowerPellet> _powerPellets;
        private readonly string _path, _layout;
        private readonly int _pelletId;
        private Vector2Int _fruitSpawnPos;
        private readonly EditorAudioSource _eatAudioSource;
        private List<Cell> _traversableCells;


        public Maze(string path, string layout, float scale, int pelletId)
        {
            _traversableCells = new List<Cell>();
            _eatAudioSource = new EditorAudioSource(Resources.Load<AudioClip>("Audio/pacman_chomp"));
            _path = path;
            _layout = layout;
            Scale = scale;
            _pelletId = pelletId;
            _initialGhostPositions = new Queue<Vector2Int>();
            _powerPellets = new List<PowerPellet>();
            _maze = Generate(layout);
            TotalPellets = PelletsEaten = 0;
            Rect = new Rect(0, 0, GetStringRepresentation()[0].Length * CELL_SIZE * Scale,
                GetStringRepresentation().Length * CELL_SIZE * Scale);
        }

        private List<List<Cell>> Generate(string layout)
        {
            var maze = new List<List<Cell>>();
            StringReader sr = new StringReader(layout);
            string line;
            int lineNumber = 0;
            while ((line = sr.ReadLine()) != null)
            {
                maze.Add(new List<Cell>());
                var split = line.Split(',');
                for (int i = 0; i < split.Length; i++)
                {
                    if (!TryConvert($"{split[i]}", out var number))
                        continue;
                    var rect = new Rect(i * CELL_SIZE * Scale, lineNumber * CELL_SIZE * Scale, CELL_SIZE * Scale,
                        CELL_SIZE * Scale);

                    var cell = new Cell(number, number > 43 || number < 0, $"{_path}{(number > 43 ? "44" : split[i])}",
                        rect);
                    cell.SetGridPosition(i,lineNumber);
                    maze[lineNumber].Add(cell);
                    SetPellet(number, cell, rect);
                    CheckPlayer(number, i, lineNumber);
                    CheckGhosts(number, i, lineNumber);
                    MarkAsFruitSpawn(cell, i, lineNumber);
                    if (number > 43 && number < 48 || number >= 100)
                        _traversableCells.Add(cell);
                }

                lineNumber++;
            }

            GenerateStringRepresentation(maze);
            return maze;
        }

        private void MarkAsFruitSpawn(Cell cell, int x, int y)
        {
            if (cell.Number != 50)
                return;
            cell.MarkAsFruitSpawn();
            _fruitSpawnPos = new Vector2Int(x, y);
        }

        private void CheckGhosts(int number, int i, int lineNumber)
        {
            if (number > 100)
                _initialGhostPositions.Enqueue(new Vector2Int(i, lineNumber));
        }

        private void CheckPlayer(int number, int i, int lineNumber)
        {
            if (number == 100)
                CurrentPlayerPos = new Vector2Int(i, lineNumber);
        }

        private void SetPellet(int number, Cell cell, Rect rect)
        {
            if (number < 0 || number >= 48 && number <= 50 || number >= 100)
                return;
            if (number == 47)
            {
                var pp = new PowerPellet(new[] {$"{_path}44", $"{_path}45", $"{_path}46", $"{_path}47"}, rect);
                pp.Eaten += PowerPelletEaten_Handler;
                _powerPellets.Add(pp);
                cell.SetPellet(pp);
                cell.SetEatSound(new EditorAudioSource(Resources.Load<AudioClip>("Audio/pacman_chomp")));
                TotalPellets++;
            }
            else if (cell.IsTraversable)
            {
                cell.SetPellet(new Pellet($"{_path}{_pelletId}", rect));
                cell.SetEatSound(_eatAudioSource);
                TotalPellets++;
            }
        }

        private bool TryConvert(string input, out int number)
        {
            try
            {
                number = Convert.ToInt32(input);
                return true;
            }
            catch (Exception)
            {
                number = 0;
                return false;
            }
        }

        private void GenerateStringRepresentation(List<List<Cell>> maze)
        {
            _stringRepresentation = new string[maze.Count][];
            for (int i = 0; i < maze.Count; i++)
            {
                _stringRepresentation[i] = new string[maze[i].Count];
                for (int j = 0; j < maze[i].Count; j++)
                {
                    _stringRepresentation[i][j] = maze[i][j].IsTraversable ? "1" : "0";
                }
            }
        }

        public void OnGui()
        {
            foreach (List<Cell> cells in _maze)
            foreach (Cell cell in cells)
                cell.OnGui();
        }

        public void SetPlayerPos(int x, int y)
        {
            CurrentPlayerPos = new Vector2Int(x, y);
            EatCell(x, y);
        }

        public void EatCell(int x, int y)
        {
            if (_maze[y][x].IsEaten)
                return;
            PelletsEaten++;
            PelletEaten?.Invoke(_maze[y][x].Points);
            _maze[y][x].Eat();
        }

        public bool Won()
        {
            return _maze.All(r => r.All(p => p.IsEaten));
        }

        public void SetFruit(Fruit fruit)
        {
            var cell = GetCellAtGridPos(_fruitSpawnPos.x, _fruitSpawnPos.y);
            fruit.SetRect(cell.GetRect());
            cell.SetPellet(fruit);
            cell.SetEatSound(new EditorAudioSource(Resources.Load<AudioClip>("Audio/pacman_eatfruit")));
        }

        public string[][] GetStringRepresentation()
        {
            return _stringRepresentation;
        }

        public Rect ReplaceInitialPlayerPosition(int replacement, out Vector2Int currentGridPosition)
        {
            currentGridPosition = CurrentPlayerPos;
            return new Rect(ReplaceCell(currentGridPosition, replacement),
                new Vector2(CELL_SIZE * Scale, CELL_SIZE * Scale));
        }

        public Rect? ReplaceInitialGhostPosition(int replacement, out Vector2Int currentGridPosition)
        {
            currentGridPosition = Vector2Int.zero;
            if (_initialGhostPositions.Count > 0)
            {
                currentGridPosition = _initialGhostPositions.Peek();
                return new Rect(ReplaceCell(_initialGhostPositions.Dequeue(), replacement),
                    new Vector2(CELL_SIZE * Scale, CELL_SIZE * Scale));
            }

            return null;
        }

        private Vector2 ReplaceCell(Vector2 gridPosition, int replacement)
        {
            var replacementCell = _maze[(int) gridPosition.y][(int) gridPosition.x];
            _maze[(int) gridPosition.y][(int) gridPosition.x] = new Cell(replacement, true, $"{_path}{replacement}",
                new Rect(replacementCell.Position.x, replacementCell.Position.y, CELL_SIZE * Scale,
                    CELL_SIZE * Scale));
            return replacementCell.Position;
        }

        public Vector2 GetCellPos(int x, int y)
        {
            return _maze[y][x].Position;
        }

        public Cell GetCellAtGridPos(int x, int y)
        {
            return _maze[y][x];
        }

        private void PowerPelletEaten_Handler(PowerPellet pellet)
        {
            pellet.Eaten -= PowerPelletEaten_Handler;
            _powerPellets.Remove(pellet);
            PowerPelletEaten?.Invoke(pellet.Points);
        }

        public void Dispose()
        {
            _powerPellets.ForEach(p => p.Eaten -= PowerPelletEaten_Handler);
            _powerPellets = null;
        }

        public void RespawnPellets()
        {
            TotalPellets = PelletsEaten = 0;
            _powerPellets.ForEach(p => p.Eaten -= PowerPelletEaten_Handler);
            _powerPellets = new List<PowerPellet>();
            foreach (List<Cell> cells in _maze)
            foreach (Cell cell in cells)
                SetPellet(cell.Number, cell, cell.GetRect());
        }

        public bool IsCellTeleporter(int x, int y, Vector2Int moveDirection, out Vector2Int newPosition)
        {
            Vector2Int? outputPos = null;
            if (x == 0 && moveDirection.x < 0 && GetCellAtGridPos(_maze[y].Count - 1, y).IsTraversable)
                outputPos = new Vector2Int(_maze[y].Count - 1, y);
            else if (x == _maze[y].Count - 1 && moveDirection.x > 0 && GetCellAtGridPos(0, y).IsTraversable)
                outputPos = new Vector2Int(0, y);
            else if (y == 0 && moveDirection.y < 0 && GetCellAtGridPos(x, _maze.Count - 1).IsTraversable)
                outputPos = new Vector2Int(x, _maze.Count - 1);
            else if (y == _maze.Count - 1 && moveDirection.y > 0 && GetCellAtGridPos(x, 0).IsTraversable)
                outputPos = new Vector2Int(x, 0);
            newPosition = outputPos ?? Vector2Int.zero;
            return outputPos != null;
        }

        public Cell GetRandomTraversableCell(int seed)
        {
            int index = new System.Random(seed).Next(0, _traversableCells.Count - 1);
            return _traversableCells[index];
        }
    }
}