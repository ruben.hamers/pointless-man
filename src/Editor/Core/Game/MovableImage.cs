using UnityEngine;

namespace HamerSoft.PointlessMan.Core
{
    public abstract class MovableImage : UpdatableImage
    {
        private const int RIGHT = 0;
        private const int LEFT = 2;
        private const int UP = 4;
        private const int Down = 6;
        private bool _currentImage;

        protected MovableImage(string path, Rect rect) : base(path, rect)
        {
        }

        protected void MoveLeft()
        {
            SwapSprite(LEFT);
        }

        protected void MoveRight()
        {
            SwapSprite(RIGHT);
        }

        protected void MoveUp()
        {
            SwapSprite(UP);
        }

        protected void MoveDown()
        {
            SwapSprite(Down);
        }

        private void SwapSprite(int index)
        {
            if (_currentImage)
                CurrentTexture = index;
            else
                CurrentTexture = index + 1;
            _currentImage = !_currentImage;
        }
    }
}