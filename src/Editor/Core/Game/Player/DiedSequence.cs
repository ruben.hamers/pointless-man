using System;
using UnityEngine;

namespace HamerSoft.PointlessMan.Core.Game.Player
{
    public class DiedSequence : UpdatableImage
    {
        public event Action Finished;
        private readonly float _interval;
        private float _time;

        public DiedSequence(string path, Rect rect, float interval) : base(path, rect)
        {
            _interval = interval;
        }

        public void Reset(Vector2 position)
        {
            Position = position;
            CurrentTexture = 0;
        }

        public override void OnGui()
        {
            if (CurrentTexture == Images.Length - 1)
            {
                Finished?.Invoke();
                CurrentTexture = 0;
                return;
            }
            base.OnGui();
            _time += DeltaTime;
            if (_time > _interval)
            {
                CurrentTexture++;
                _time = 0;
            }

            // _time += DeltaTime;
            // if (_time > _interval)
            // {
            //     if (CurrentTexture == Images.Length - 1)
            //     {
            //         Finished?.Invoke();
            //         CurrentTexture = 0;
            //     }
            //     else
            //         CurrentTexture++;
            //     _time = 0;
            // }
        }
    }
}