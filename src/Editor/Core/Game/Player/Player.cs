using System;
using HamerSoft.EditorAudio;
using HamerSoft.PointlessMan.Core.Extensions;
using UnityEngine;

namespace HamerSoft.PointlessMan.Core.Game.Player
{
    public class Player : MovableImage, IMovable, IPausable
    {
        public event Action<IMovable> Moved, Teleported;
        public event Action Died;
        public Vector2Int CurrentPosition => new Vector2Int(_x, _y);
        public bool IsDead { get; private set; }
        private readonly Maze _maze;
        private int _x, _y;
        private float _speed;
        private Vector2 _targetPosition;
        private bool _paused;
        private Vector2Int _startPosition, _targetGridPosition, _currentMoveDirection;
        private KeyCode _keyCode;
        private EditorAudioSource _deathAudioSource;
        private readonly DiedSequence _diedSequence;

        public Player(string path, Rect rect, Maze maze) : base(path, rect)
        {
            _maze = maze;
            _keyCode = KeyCode.None;
            Rect = maze.ReplaceInitialPlayerPosition(44, out var currentGridPosition);
            _x = currentGridPosition.x;
            _y = currentGridPosition.y;
            _startPosition = currentGridPosition;
            _speed = 1;
            _currentMoveDirection = DetermineStartMoveDirection();
            _targetGridPosition = currentGridPosition + _currentMoveDirection;
            _targetPosition = maze.GetCellPos(_targetGridPosition.x, _targetGridPosition.y);
            _diedSequence = new DiedSequence(path.Replace("Player", "Dead"), Rect, .1f);
        }

        private Vector2Int DetermineStartMoveDirection()
        {
            if (CanMoveLeft())
                return new Vector2Int(-1, 0);
            else if (CanMoveRight())
                return new Vector2Int(1, 0);
            else if (CanMoveUp())
                return new Vector2Int(0, -1);
            else if (CanMoveDown())
                return new Vector2Int(0, 1);
            return new Vector2Int(0, 0);
        }

        public void SetMoveSpeed(float speed = 1)
        {
            _speed = speed;
        }

        public void Reset()
        {
            CurrentTexture = 0;
            IsDead = false;
            _x = _startPosition.x;
            _y = _startPosition.y;
            Position = _maze.GetCellPos(_x, _y);
            _currentMoveDirection = DetermineStartMoveDirection();
        }

        public override void OnGui()
        {
            base.OnGui();
            if (IsDead)
                _diedSequence.OnGui();
            else if (!_paused)
            {
                _keyCode = Event.current.isKey ? Event.current.keyCode : _keyCode;
                Move();
            }
        }

        private void Move()
        {
            Position = Vector2.MoveTowards(Position, _targetPosition, _speed);
            if (Position.AlmostEquals(_targetPosition, .1f))
            {
                _x = _targetGridPosition.x;
                _y = _targetGridPosition.y;
                OnMoved();
                ChangeDirection();
                Animate();
                if (CanMoveInCurrentDirection(out var gridPos))
                {
                    _targetGridPosition = gridPos;
                    _targetPosition = _maze.GetCellPos(gridPos.x, gridPos.y);
                }
            }
        }

        private void Animate()
        {
            if (_currentMoveDirection == new Vector2Int(-1, 0))
                MoveLeft();
            else if (_currentMoveDirection == new Vector2Int(1, 0))
                MoveRight();
            else if (_currentMoveDirection == new Vector2Int(0, -1))
                MoveUp();
            else if (_currentMoveDirection == new Vector2Int(0, 1))
                MoveDown();
        }

        private void ChangeDirection()
        {
            if (_keyCode == KeyCode.None)
                return;
            var oldMoveDir = _currentMoveDirection;
            switch (_keyCode)
            {
                case KeyCode.W:
                case KeyCode.UpArrow:
                    if (CanMoveUp())
                        _currentMoveDirection = new Vector2Int(0, -1);
                    break;
                case KeyCode.S:
                case KeyCode.DownArrow:
                    if (CanMoveDown())
                        _currentMoveDirection = new Vector2Int(0, 1);
                    break;
                case KeyCode.A:
                case KeyCode.LeftArrow:
                    if (CanMoveLeft())
                        _currentMoveDirection = new Vector2Int(-1, 0);
                    break;
                case KeyCode.D:
                case KeyCode.RightArrow:
                    if (CanMoveRight())
                        _currentMoveDirection = new Vector2Int(1, 0);
                    break;
            }

            if (_currentMoveDirection != oldMoveDir)
            {
                CanMoveInCurrentDirection(out var gridPos);
                _targetGridPosition = gridPos;
                _targetPosition = _maze.GetCellPos(gridPos.x, gridPos.y);
                _keyCode = KeyCode.None;
            }
        }

        private bool CanMoveInCurrentDirection(out Vector2Int gridPos)
        {
            if (_maze.IsCellTeleporter(_x, _y, _currentMoveDirection, out var teleportPos))
            {
                _x = teleportPos.x;
                _y = teleportPos.y;
                Position = _maze.GetCellPos(_x, _y);
                OnMoved();
                Animate();
                gridPos = teleportPos;
                OnTeleported();
                return true;
            }

            Vector2Int newPosition = CurrentPosition + _currentMoveDirection;
            bool inRangeY = newPosition.y >= 0 && newPosition.y < _maze.GetStringRepresentation().Length;

            bool inRangeX = inRangeY && newPosition.x >= 0 &&
                            newPosition.x < _maze.GetStringRepresentation()[newPosition.y].Length;

            bool IsTraversable =
                inRangeY && inRangeX && _maze.GetCellAtGridPos(newPosition.x, newPosition.y).IsTraversable;
            if (IsTraversable)
                gridPos = newPosition;
            else
                gridPos = CurrentPosition;

            return IsTraversable;
        }

        private bool CanMoveLeft()
        {
            bool canMove = _x > 0 && _maze.GetStringRepresentation()[_y][_x - 1] == "1";
            return canMove;
        }

        private bool CanMoveRight()
        {
            bool canMove = _x < _maze.GetStringRepresentation()[_y].Length - 1 &&
                           _maze.GetStringRepresentation()[_y][_x + 1] == "1";
            return canMove;
        }

        private bool CanMoveUp()
        {
            bool canMove = _y > 0 && _maze.GetStringRepresentation()[_y - 1][_x] == "1";
            return canMove;
        }

        private bool CanMoveDown()
        {
            bool canMove = _y < _maze.GetStringRepresentation().Length - 1 &&
                           _maze.GetStringRepresentation()[_y + 1][_x] == "1";
            return canMove;
        }

        private void OnMoved()
        {
            _maze.SetPlayerPos(_x, _y);
            Moved?.Invoke(this);
        }

        private void OnTeleported()
        {
            Teleported?.Invoke(this);
        }

        public void Pause(bool pause = true)
        {
            _paused = pause;
        }

        public void SetDeathSound(AudioClip clip)
        {
            _deathAudioSource = new EditorAudioSource(clip);
        }

        public void Die()
        {
            CurrentTexture = -1;
            _diedSequence.Reset(Position);
            _deathAudioSource?.Play();
            _diedSequence.Finished += DiedSequenceOnFinished_Handler;
            IsDead = true;
        }

        private void DiedSequenceOnFinished_Handler()
        {
            _diedSequence.Finished -= DiedSequenceOnFinished_Handler;
            Died?.Invoke();
        }
    }
}