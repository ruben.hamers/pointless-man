using System.Text;
using HamerSoft.PointlessMan.Core.Game;

namespace HamerSoft.PointlessMan.Core
{
    public static class InMemoryMaze
    {
        public static Maze Get(int scale = 2)
        {
            var sb = new StringBuilder();
            sb.AppendLine("1,10,10,10,10,10,10,10,10,10,10,10,10,43,42,10,10,10,10,10,10,10,10,10,10,10,10,0");
            sb.AppendLine("3,44,44,44,44,44,44,44,44,44,44,44,44,25,24,44,44,44,44,44,44,44,44,44,44,44,44,2");
            sb.AppendLine("3,44,23,14,14,22,44,23,14,14,14,22,44,25,24,44,23,14,14,14,22,44,23,14,14,22,44,2");
            sb.AppendLine("3,47,25,49,49,24,44,25,49,49,49,24,44,25,24,44,25,49,49,49,24,44,25,49,49,24,47,2");
            sb.AppendLine("3,44,27,20,20,26,44,27,20,20,20,26,44,27,26,44,27,20,20,20,26,44,27,20,20,26,44,2");
            sb.AppendLine("3,44,44,44,44,44,44,44,44,44,44,44,44,44,44,44,44,44,44,44,44,44,44,44,44,44,44,2");
            sb.AppendLine("3,44,23,14,14,22,44,23,22,44,23,14,14,14,14,14,14,22,44,23,22,44,23,14,14,22,44,2");
            sb.AppendLine("3,44,27,20,20,26,44,25,24,44,27,20,20,22,23,20,20,26,44,25,24,44,27,20,20,26,44,2");
            sb.AppendLine("3,44,44,44,44,44,44,25,24,44,44,44,44,25,24,44,44,44,44,25,24,44,44,44,44,44,44,2");
            sb.AppendLine("5,12,12,12,12,22,44,25,27,14,14,22,49,25,24,49,23,14,14,26,24,44,23,12,12,12,12,4");
            sb.AppendLine("49,49,49,49,49,3,44,25,23,20,20,26,49,27,26,49,27,20,20,22,24,44,2,49,49,49,49,49");
            sb.AppendLine("49,49,49,49,49,3,44,25,24,49,49,49,49,49,49,49,49,49,49,25,24,44,2,49,49,49,49,49");
            sb.AppendLine("49,49,49,49,49,3,44,25,24,49,29,13,13,-1,-1,13,13,28,49,25,24,44,2,49,49,49,49,49");
            sb.AppendLine("10,10,10,10,10,26,44,27,26,49,2,49,49,49,49,49,49,3,49,27,26,44,27,10,10,10,10,10");
            sb.AppendLine("49,49,49,49,49,49,44,49,49,49,2,101,49,102,49,103,104,3,49,49,49,44,49,49,49,49,49,49");
            sb.AppendLine("12,12,12,12,12,22,44,23,22,49,2,49,49,49,49,49,49,3,49,23,22,44,23,12,12,12,12,12");
            sb.AppendLine("49,49,49,49,49,3,44,25,24,49,31,10,10,10,10,10,10,30,49,25,24,44,2,49,49,49,49,49");
            sb.AppendLine("49,49,49,49,49,3,44,25,24,49,49,49,49,50,49,49,49,49,49,25,24,44,2,49,49,49,49,49");
            sb.AppendLine("49,49,49,49,49,3,44,25,24,49,23,14,14,14,14,14,14,22,49,25,24,44,2,49,49,49,49,49");
            sb.AppendLine("1,10,10,10,10,26,44,27,26,49,27,20,20,22,23,20,20,26,49,27,26,44,27,10,10,10,10,0");
            sb.AppendLine("3,44,44,44,44,44,44,44,44,44,44,44,44,25,24,44,44,44,44,44,44,44,44,44,44,44,44,2");
            sb.AppendLine("3,44,23,14,14,22,44,23,14,14,14,22,44,25,24,44,23,14,14,14,22,44,23,14,14,22,44,2");
            sb.AppendLine("3,44,27,20,22,24,44,27,20,20,20,26,44,27,26,44,27,20,20,20,26,44,25,23,20,26,44,2");
            sb.AppendLine("3,47,44,44,25,24,44,44,44,44,44,44,44,100,49,44,44,44,44,44,44,44,25,24,44,44,47,2");
            sb.AppendLine("27,14,22,44,25,24,44,23,22,44,23,14,14,14,14,14,14,22,44,23,22,44,25,24,44,23,14,26");
            sb.AppendLine("23,20,26,44,27,26,44,25,24,44,27,20,20,22,23,20,20,26,44,25,24,44,27,26,44,27,20,22");
            sb.AppendLine("3,44,44,44,44,44,44,25,24,44,44,44,44,25,24,44,44,44,44,25,24,44,44,44,44,44,44,2");
            sb.AppendLine("3,44,23,14,14,14,14,26,27,14,14,22,44,25,24,44,23,14,14,26,27,14,14,14,14,22,44,2");
            sb.AppendLine("3,44,27,20,20,20,20,20,20,20,20,26,44,27,26,44,27,20,20,20,20,20,20,20,20,26,44,2");
            sb.AppendLine("3,44,44,44,44,44,44,44,44,44,44,44,44,44,44,44,44,44,44,44,44,44,44,44,44,44,44,2");
            sb.AppendLine("5,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,4");

            return new Maze("Scene/Maze/maze_", sb.ToString(), scale, 45);
        }
    }
}