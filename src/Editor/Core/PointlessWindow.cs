﻿using System;
using HamerSoft.EditorAudio;
using HamerSoft.PointlessMan.Core.Game;
using UnityEditor;
using UnityEngine;

namespace HamerSoft.PointlessMan.Core
{
    public class PointlessWindow : EditorWindow
    {
        private static PointlessWindow _pointlessWindow;
        private static GameController _gameController;
        private static string _layout;

        [MenuItem("HamerSoft/PointlessMan/Play")]
        private static void ShowWindow()
        {
            _gameController?.Dispose();
            _pointlessWindow = GetWindow<PointlessWindow>();
            _pointlessWindow.titleContent = new GUIContent("PointlessMan");
            _pointlessWindow.minSize = _gameController?.GetMinWindowSize() ?? new Vector2(400, 400);
            _pointlessWindow.Show();
        }

        public static void Show(string layout)
        {
            _layout = layout;
            ShowWindow();
        }

        protected void Awake()
        {
            StartGame();
        }

        private void StartGame()
        {
            try
            {
                var maze = string.IsNullOrEmpty(_layout)
                    ? InMemoryMaze.Get()
                    : new Maze("Scene/Maze/maze_", _layout, 2, 45);
                _gameController = new GameController(maze);
                _gameController.GameOver += GameControllerOnGameOver_Handler;
                _gameController.GameWon += GameControllerOnGameWon_Handler;
                _gameController.Start();
            }
            catch (Exception _)
            {
                ShowNotification(new GUIContent("Failed to parse custom input file!"));
                DisposeController();
                _pointlessWindow.Close();
                DestroyImmediate(_pointlessWindow);
            }
        }

        private void GameControllerOnGameWon_Handler()
        {
            if (EditorUtility.DisplayDialog("Dayum!",
                "You've Beaten all 256 levels. You surely want to do this again, right?", "Well ofc!",
                "No"))
            {
                DisposeController();
                StartGame();
            }
            else
            {
                EditorAudioSource.StopAllClips();
                DisposeController();
                _pointlessWindow.Close();
                DestroyImmediate(_pointlessWindow);
            }
        }

        private void GameControllerOnGameOver_Handler()
        {
            if (EditorUtility.DisplayDialog("Too Bad...", "You lost the game! Do you want to try again?", "Yes",
                "No"))
            {
                DisposeController();
                StartGame();
            }
            else
            {
                EditorAudioSource.StopAllClips();
                DisposeController();
                _pointlessWindow.Close();
                DestroyImmediate(_pointlessWindow);
            }
        }

        private void OnDestroy()
        {
            EditorAudioSource.StopAllClips();
            DisposeController();
        }

        private void DisposeController()
        {
            if (_gameController == null)
                return;
            _gameController.GameOver -= GameControllerOnGameOver_Handler;
            _gameController?.Dispose();
            _gameController = null;
            _layout = null;
        }

        private void OnGUI()
        {
            FocusWindowIfItsOpen<PointlessWindow>();
            _gameController?.OnGui();
        }
    }
}