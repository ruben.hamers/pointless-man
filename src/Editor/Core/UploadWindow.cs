using UnityEditor;
using UnityEngine;

namespace HamerSoft.PointlessMan.Core
{
    public class UploadWindow : EditorWindow
    {
        private static UploadWindow _window;
        private TextAsset _file;

        [MenuItem("HamerSoft/PointlessMan/Upload")]
        private static void ShowWindow()
        {
            _window = GetWindow<UploadWindow>();
            _window.titleContent = new GUIContent("PointlessMan");
            _window.minSize = new Vector2(200, 100);
            _window.Show();
        }

        private void OnGUI()
        {
            _file = EditorGUILayout.ObjectField("Maze layout", _file, typeof(TextAsset), false) as TextAsset;
            if (GUILayout.Button("Parse file") && _file != null)
            {
                PointlessWindow.Show(_file.text);
                _window.Close();
                DestroyImmediate(_window);
            }
        }
    }
}