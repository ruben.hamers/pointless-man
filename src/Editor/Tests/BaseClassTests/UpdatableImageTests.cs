using System.Collections;
using System.Diagnostics;
using HamerSoft.PointlessMan.Core;
using HamerSoft.PointlessMan.Core.Extensions;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace PointlessMan.Tests.BaseClassTests
{
    public class UpdatableImageTests
    {
        private class MockUpdatableImage : UpdatableImage
        {
            public MockUpdatableImage(string path, Rect rect) : base(path, rect)
            {
            }

            public MockUpdatableImage(string[] path, Rect rect) : base(path, rect)
            {
            }

            public float DeltaTime => base.DeltaTime;
        }

        [UnityTest]
        public IEnumerator DeltaTimeIsCalculatedCorrectly()
        {
            var mock = new MockUpdatableImage("foo/bar", new Rect());
            mock.OnGui();
            var stopWatch = Stopwatch.StartNew();
            while (stopWatch.Elapsed.TotalSeconds <= 1)
                yield return null;
            mock.OnGui();
            Assert.True(mock.DeltaTime.AlmostEquals(1, .05f));
        }
    }
}