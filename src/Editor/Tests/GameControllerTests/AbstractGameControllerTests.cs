using System.Collections.Generic;
using System.Reflection;
using HamerSoft.PointlessMan.Core;
using HamerSoft.PointlessMan.Core.Game;
using HamerSoft.PointlessMan.Core.Game.Player;
using UnityEngine;

namespace PointlessMan.Tests.GameControllerTests
{
    public abstract class AbstractGameControllerTests
    {
        protected class MockGameController : GameController
        {
            public MockGameController(Maze maze) : base(maze)
            {
            }

            public List<Ghost> GetGhosts()
            {
                return Ghosts;
            }

            public Player GetPlayer()
            {
                return Player;
            }

            public Maze GetMaze()
            {
                return Maze;
            }

            public void ForceUpdatePosition<T>(T movable, Vector2Int currentPosition) where T : IMovable
            {
                typeof(T).GetField("_x", BindingFlags.Instance | BindingFlags.NonPublic)
                    .SetValue(movable, currentPosition.x);
                typeof(T).GetField("_y", BindingFlags.Instance | BindingFlags.NonPublic)
                    .SetValue(movable, currentPosition.y);
            }

            public float GetGhostSpeed(Ghost ghost)
            {
                return (float) typeof(Ghost).GetField("_speed", BindingFlags.Instance | BindingFlags.NonPublic)
                    .GetValue(ghost);
            }

            public void ForceOnMoved<T>(T movable) where T : IMovable
            {
                typeof(T).GetMethod("OnMoved", BindingFlags.Instance | BindingFlags.NonPublic)
                    .Invoke(movable, null);
            }

            public void ForceOnTeleported<T>(T movable) where T : IMovable
            {
                typeof(T).GetMethod("OnTeleported", BindingFlags.Instance | BindingFlags.NonPublic)
                    .Invoke(movable, null);
            }

            public int GetLives()
            {
                return (int) typeof(GameController).GetField("_lives", BindingFlags.Instance | BindingFlags.NonPublic)
                    .GetValue(this);
            }

            public void SetCurrentLevel(int i)
            {
                typeof(GameController)
                    .GetField("_currentLevel", BindingFlags.Instance | BindingFlags.NonPublic)
                    .SetValue(this, i);
            }

            public int GetCurrentLevel()
            {
                return (int) typeof(GameController)
                    .GetField("_currentLevel", BindingFlags.Instance | BindingFlags.NonPublic)
                    .GetValue(this);
            }

            public int GetScore()
            {
                return (int) typeof(GameController)
                    .GetProperty("_score", BindingFlags.Instance | BindingFlags.NonPublic)
                    .GetValue(this);
            }

            public void SetScore(int score)
            {
                typeof(GameController)
                    .GetProperty("_score", BindingFlags.Instance | BindingFlags.NonPublic)
                    .SetValue(this, score);
            }

            public void SetGhostsInSuccession(int i)
            {
                typeof(GameController)
                    .GetField("_ghostsEatenInSuccession", BindingFlags.Instance | BindingFlags.NonPublic)
                    .SetValue(this, i);
            }

            public void SetPelletsInSuccession(int i)
            {
                typeof(GameController)
                    .GetField("_pelletsEatenInSuccession", BindingFlags.Instance | BindingFlags.NonPublic)
                    .SetValue(this, i);

                typeof(Maze).GetProperty("TotalPellets", BindingFlags.Instance | BindingFlags.Public)
                    .SetValue(Maze, 244);
                typeof(Maze).GetProperty("PelletsEaten", BindingFlags.Instance | BindingFlags.Public).SetValue(Maze, i);
            }

            public IEatable GetPelletAtPos(int x, int y)
            {
                return typeof(Cell).GetField("_pellet", BindingFlags.Instance | BindingFlags.NonPublic)
                    .GetValue(Maze.GetCellAtGridPos(x, y)) as IEatable;
            }
        }
    }
}