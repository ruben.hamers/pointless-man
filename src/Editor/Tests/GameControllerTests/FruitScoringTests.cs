using HamerSoft.PointlessMan.Core.Game;
using NUnit.Framework;

namespace PointlessMan.Tests.GameControllerTests
{
    public class FruitScoringTests : AbstractGameControllerTests
    {
        private MockGameController _controller;

        [Test]
        public void AfterEating_64_Pellets_InLevel_0_NO_Fruit_Appears()
        {
            var maze = new Maze("Scene/Maze/maze_", "100,101,44,50", 3, 45);
            _controller = new MockGameController(maze);
            _controller.SetPelletsInSuccession(63);
            maze.EatCell(2, 0);
            Assert.True(maze.GetCellAtGridPos(3, 0).IsFruitSpawn && _controller.GetPelletAtPos(3, 0) is null);
        }

        [Test]
        public void When_There_Are_66_pellets_Left_InLevel_0_NO_Fruit_Appears()
        {
            var maze = new Maze("Scene/Maze/maze_", "100,101,44,50", 3, 45);
            _controller = new MockGameController(maze);
            _controller.SetPelletsInSuccession(177);
            maze.EatCell(2, 0);
            Assert.True(maze.GetCellAtGridPos(3, 0).IsFruitSpawn && _controller.GetPelletAtPos(3, 0) is null);
        }

        [Test]
        public void AfterEating_64_Pellets_Fruit_Appears()
        {
            var maze = new Maze("Scene/Maze/maze_", "100,101,44,50", 3, 45);
            _controller = new MockGameController(maze);
            _controller.SetPelletsInSuccession(63);
            _controller.SetCurrentLevel(1);
            maze.EatCell(2, 0);
            Assert.True(maze.GetCellAtGridPos(3, 0).IsFruitSpawn && _controller.GetPelletAtPos(3, 0) is Fruit);
        }

        [Test]
        public void When_There_Are_66_pellets_Left_Another_Fruit_Appears()
        {
            var maze = new Maze("Scene/Maze/maze_", "100,101,44,50", 3, 45);
            _controller = new MockGameController(maze);
            _controller.SetPelletsInSuccession(177);
            _controller.SetCurrentLevel(1);
            maze.EatCell(2, 0);
            Assert.True(maze.GetCellAtGridPos(3, 0).IsFruitSpawn && _controller.GetPelletAtPos(3, 0) is Fruit);
        }

        [Test]
        public void Level_1_Will_Spawn_Cherry()
        {
            var maze = new Maze("Scene/Maze/maze_", "100,101,44,44,44,50", 3, 45);
            _controller = new MockGameController(maze);
            _controller.SetCurrentLevel(1);
            EatCellAndAssertFruit<Cherry>(maze, 2, 63);
            EatCellAndAssertFruit<Cherry>(maze, 3, 177);
        }

        [Test]
        public void Level_2_Will_Spawn_Strawberry()
        {
            var maze = new Maze("Scene/Maze/maze_", "100,101,44,44,44,50", 3, 45);
            _controller = new MockGameController(maze);
            _controller.SetCurrentLevel(2);
            EatCellAndAssertFruit<Strawberry>(maze, 2, 63);
            EatCellAndAssertFruit<Strawberry>(maze, 3, 177);
        }


        [Test]
        public void Level_3_And_4_Will_Spawn_Orange()
        {
            var maze = new Maze("Scene/Maze/maze_", "100,101,44,44,44,50,44,44", 3, 45);
            _controller = new MockGameController(maze);
            _controller.SetCurrentLevel(3);
            EatCellAndAssertFruit<Orange>(maze, 2, 63);
            EatCellAndAssertFruit<Orange>(maze, 3, 177);
            _controller.SetCurrentLevel(4);
            EatCellAndAssertFruit<Orange>(maze, 6, 63);
            EatCellAndAssertFruit<Orange>(maze, 7, 177);
        }

        [Test]
        public void Level_5_And_6_Will_Spawn_Apple()
        {
            var maze = new Maze("Scene/Maze/maze_", "100,101,44,44,44,50,44,44", 3, 45);
            _controller = new MockGameController(maze);
            _controller.SetCurrentLevel(5);
            EatCellAndAssertFruit<Apple>(maze, 2, 63);
            EatCellAndAssertFruit<Apple>(maze, 3, 177);
            _controller.SetCurrentLevel(6);
            EatCellAndAssertFruit<Apple>(maze, 6, 63);
            EatCellAndAssertFruit<Apple>(maze, 7, 177);
        }

        [Test]
        public void Level_7_And_8_Will_Spawn_Melon()
        {
            var maze = new Maze("Scene/Maze/maze_", "100,101,44,44,44,50,44,44", 3, 45);
            _controller = new MockGameController(maze);
            _controller.SetCurrentLevel(7);
            EatCellAndAssertFruit<Melon>(maze, 2, 63);
            EatCellAndAssertFruit<Melon>(maze, 3, 177);
            _controller.SetCurrentLevel(8);
            EatCellAndAssertFruit<Melon>(maze, 6, 63);
            EatCellAndAssertFruit<Melon>(maze, 7, 177);
        }

        [Test]
        public void Level_9_And_10_Will_Spawn_Galaxian()
        {
            var maze = new Maze("Scene/Maze/maze_", "100,101,44,44,44,50,44,44", 3, 45);
            _controller = new MockGameController(maze);
            _controller.SetCurrentLevel(9);
            EatCellAndAssertFruit<Galaxian>(maze, 2, 63);
            EatCellAndAssertFruit<Galaxian>(maze, 3, 177);
            _controller.SetCurrentLevel(10);
            EatCellAndAssertFruit<Galaxian>(maze, 6, 63);
            EatCellAndAssertFruit<Galaxian>(maze, 7, 177);
        }

        [Test]
        public void Level_11_And_12_Will_Spawn_Bell()
        {
            var maze = new Maze("Scene/Maze/maze_", "100,101,44,44,44,50,44,44", 3, 45);
            _controller = new MockGameController(maze);
            _controller.SetCurrentLevel(11);
            EatCellAndAssertFruit<Bell>(maze, 2, 63);
            EatCellAndAssertFruit<Bell>(maze, 3, 177);
            _controller.SetCurrentLevel(12);
            EatCellAndAssertFruit<Bell>(maze, 6, 63);
            EatCellAndAssertFruit<Bell>(maze, 7, 177);
        }

        [Test]
        public void Level_13_And_More_Will_Spawn_Key()
        {
            var maze = new Maze("Scene/Maze/maze_", "100,101,44,44,44,50,44,44,44,44", 3, 45);
            _controller = new MockGameController(maze);
            _controller.SetCurrentLevel(13);
            EatCellAndAssertFruit<Key>(maze, 2, 63);
            EatCellAndAssertFruit<Key>(maze, 3, 177);
            _controller.SetCurrentLevel(14);
            EatCellAndAssertFruit<Key>(maze, 6, 63);
            EatCellAndAssertFruit<Key>(maze, 7, 177);
            _controller.SetCurrentLevel(99);
            EatCellAndAssertFruit<Key>(maze, 8, 63);
            EatCellAndAssertFruit<Key>(maze, 9, 177);
        }

        [Test]
        public void Eating_1_Cherry_Gives_100_Points()
        {
            var maze = new Maze("Scene/Maze/maze_", "100,101,44,44,44,50,44,44,44,44", 3, 45);
            _controller = new MockGameController(maze);
            _controller.SetCurrentLevel(1);
            EatCellAndAssertFruit<Cherry>(maze, 2, 63);
            Assert.AreEqual(110, _controller.GetScore());
        }

        [Test]
        public void Eating_1_Strawberry_Gives_300_Points()
        {
            var maze = new Maze("Scene/Maze/maze_", "100,101,44,44,44,50,44,44,44,44", 3, 45);
            _controller = new MockGameController(maze);
            _controller.SetCurrentLevel(2);
            EatCellAndAssertFruit<Strawberry>(maze, 2, 63);
            Assert.AreEqual(310, _controller.GetScore());
        }

        [Test]
        public void Eating_1_Orange_Gives_500_Points()
        {
            var maze = new Maze("Scene/Maze/maze_", "100,101,44,44,44,50,44,44,44,44", 3, 45);
            _controller = new MockGameController(maze);
            _controller.SetCurrentLevel(3);
            EatCellAndAssertFruit<Orange>(maze, 2, 63);
            Assert.AreEqual(510, _controller.GetScore());
        }

        [Test]
        public void Eating_1_Apple_Gives_700_Points()
        {
            var maze = new Maze("Scene/Maze/maze_", "100,101,44,44,44,50,44,44,44,44", 3, 45);
            _controller = new MockGameController(maze);
            _controller.SetCurrentLevel(5);
            EatCellAndAssertFruit<Apple>(maze, 2, 63);
            Assert.AreEqual(710, _controller.GetScore());
        }

        [Test]
        public void Eating_1_Melon_Gives_1000_Points()
        {
            var maze = new Maze("Scene/Maze/maze_", "100,101,44,44,44,50,44,44,44,44", 3, 45);
            _controller = new MockGameController(maze);
            _controller.SetCurrentLevel(7);
            EatCellAndAssertFruit<Melon>(maze, 2, 63);
            Assert.AreEqual(1010, _controller.GetScore());
        }

        [Test]
        public void Eating_1_Galaxian_Gives_2000_Points()
        {
            var maze = new Maze("Scene/Maze/maze_", "100,101,44,44,44,50,44,44,44,44", 3, 45);
            _controller = new MockGameController(maze);
            _controller.SetCurrentLevel(9);
            EatCellAndAssertFruit<Galaxian>(maze, 2, 63);
            Assert.AreEqual(2010, _controller.GetScore());
        }

        [Test]
        public void Eating_1_Bell_Gives_3000_Points()
        {
            var maze = new Maze("Scene/Maze/maze_", "100,101,44,44,44,50,44,44,44,44", 3, 45);
            _controller = new MockGameController(maze);
            _controller.SetCurrentLevel(11);
            EatCellAndAssertFruit<Bell>(maze, 2, 63);
            Assert.AreEqual(3010, _controller.GetScore());
        }

        [Test]
        public void Eating_1_Key_Gives_5000_Points()
        {
            var maze = new Maze("Scene/Maze/maze_", "100,101,44,44,44,50,44,44,44,44", 3, 45);
            _controller = new MockGameController(maze);
            _controller.SetCurrentLevel(13);
            EatCellAndAssertFruit<Key>(maze, 2, 63);
            Assert.AreEqual(5010, _controller.GetScore());
        }

        [TearDown]
        public void TearDown()
        {
            _controller.Dispose();
            _controller = null;
        }

        private void EatCellAndAssertFruit<T>(Maze maze, int xPos, int inSuccession) where T : Fruit
        {
            _controller.SetPelletsInSuccession(inSuccession);
            maze.EatCell(xPos, 0);
            Assert.True(_controller.GetPelletAtPos(5, 0) is T);
            maze.EatCell(5, 0);
        }
    }
}