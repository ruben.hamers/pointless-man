using System.Collections;
using System.Linq;
using HamerSoft.PointlessMan.Core.Game;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace PointlessMan.Tests.GameControllerTests
{
    public class GameOverTests : AbstractGameControllerTests
    {
        private MockGameController _controller;
        private bool _playerDied;

        [SetUp]
        public void Setup()
        {
            _playerDied = false;
        }

        [Test]
        public void GameIsNotLostWhen_GhostEqualsPlayerPosition_While_NotScared_And_NotEaten_AndLivesAre_GreaterThan_0()
        {
            var maze = new Maze("Scene/Maze/maze_", "100,101,44", 3, 45);
            _controller = new MockGameController(maze);
            _controller.GameOver += GameLost;
            _controller.ForceUpdatePosition(_controller.GetGhosts().First(), _controller.GetPlayer().CurrentPosition);
            _controller.ForceOnMoved(_controller.GetGhosts().First());
            _controller.GameOver -= GameLost;
            Assert.Greater(3, _controller.GetLives());
        }

        [Test]
        public void When_GhostEqualsPlayerPosition_ThePlayer_Looses_A_Live()
        {
            var maze = new Maze("Scene/Maze/maze_", "100,101,44", 3, 45);
            _controller = new MockGameController(maze);
            _controller.ForceUpdatePosition(_controller.GetGhosts().First(), _controller.GetPlayer().CurrentPosition);
            _controller.ForceOnMoved(_controller.GetGhosts().First());
            Assert.Greater(3, _controller.GetLives());
        }

        [Test]
        public void GameIsLostWhen_GhostEqualsPlayerPosition_While_NotScared_And_NotEaten_AndLivesAre_0()
        {
            var maze = new Maze("Scene/Maze/maze_", "100,101,44", 3, 45);
            _controller = new MockGameController(maze);
            _controller.GameOver += GameLost;
            for (int i = 0; i < 3; i++)
            {
                _controller.ForceUpdatePosition(_controller.GetGhosts().First(),
                    _controller.GetPlayer().CurrentPosition);
                _controller.ForceOnMoved(_controller.GetGhosts().First());
            }
        }

        [Test]
        public void GameIsNotLostWhen_GhostEqualsPlayerPosition_While_Scared_And_IsEaten()
        {
            var maze = new Maze("Scene/Maze/maze_", "100,101,44", 3, 45);
            _controller = new MockGameController(maze);
            var ghost = _controller.GetGhosts().First();
            var player = _controller.GetPlayer();
            ghost.ActScared(1);
            _controller.ForceUpdatePosition(ghost, player.CurrentPosition);
            _controller.ForceOnMoved(ghost);

            Assert.True(ghost.CurrentPosition == player.CurrentPosition
                        && !ghost.IsScared && ghost.IsEaten);
        }

        [Test]
        public void GameIsNotLostWhen_GhostEqualsPlayerPosition_While_NotScared_And_Eaten()
        {
            var maze = new Maze("Scene/Maze/maze_", "100,101,44", 3, 45);
            _controller = new MockGameController(maze);
            var ghost = _controller.GetGhosts().First();
            var player = _controller.GetPlayer();
            ghost.ActScared(1);
            ghost.Eat();
            _controller.ForceUpdatePosition(ghost, player.CurrentPosition);
            _controller.ForceOnMoved(ghost);
            Assert.True(ghost.CurrentPosition == player.CurrentPosition
                        && !ghost.IsScared && ghost.IsEaten);
        }

        [Test]
        public void LevelIncrements_When_All_Pellets_Are_Eaten()
        {
            var maze = new Maze("Scene/Maze/maze_", "101,100,44", 3, 45);
            _controller = new MockGameController(maze);
            var player = _controller.GetPlayer();
            _controller.ForceUpdatePosition(player, new Vector2Int(2, 0));
            _controller.ForceOnMoved(player);
            Assert.Greater(_controller.GetCurrentLevel(), 0);
        }

        [Test]
        public void WhenLevel_256_IsReached_TheGameIs_Won()
        {
            var maze = new Maze("Scene/Maze/maze_", "101,100,44", 3, 45);
            _controller = new MockGameController(maze);
            _controller.GameWon += GameWon;
            var player = _controller.GetPlayer();
            for (int i = 0; i < 256; i++)
            {
                for (int j = 0; j < 3; j++)
                    maze.EatCell(j, 0);
                _controller.ForceUpdatePosition(player, new Vector2Int(2, 0));
                _controller.ForceOnMoved(player);
            }

            Assert.AreEqual(255, _controller.GetCurrentLevel());
            _controller.GameWon -= GameWon;
        }

        private void GameLost()
        {
            Debug.Log($"Gamover");
            _controller.GameOver -= GameLost;
            Assert.True(true);
        }

        private void GameWon()
        {
            Debug.Log($"Game won!");
            _controller.GameWon -= GameWon;
            Assert.True(true);
        }

        private void ControllerOnPlayerDied()
        {
            Debug.Log($"Player Died");
            _controller.PlayerDied -= ControllerOnPlayerDied;
            _playerDied = true;
        }

        [TearDown]
        public void TearDown()
        {
            _controller?.Dispose();
        }
    }
}