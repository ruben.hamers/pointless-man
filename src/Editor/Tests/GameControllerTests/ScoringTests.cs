using System.Linq;
using HamerSoft.PointlessMan.Core.Game;
using NUnit.Framework;
using UnityEditor;
using UnityEngine;

namespace PointlessMan.Tests.GameControllerTests
{
    public class ScoringTests : AbstractGameControllerTests
    {
        private MockGameController _controller;

        [Test]
        public void Eating_1_Pellet_Grants_10_Points()
        {
            var maze = new Maze("Scene/Maze/maze_", "100,101,44", 3, 45);
            _controller = new MockGameController(maze);
                maze.EatCell(2, 0);
           Assert.AreEqual(10, _controller.GetScore());
        }

        [Test]
        public void Eating_10_Pellet_Grants_100_Points()
        {
            var maze = new Maze("Scene/Maze/maze_", "100,101,44,44,44,44,44,44,44,44,44,44", 3, 45);
            _controller = new MockGameController(maze);
            for (int i = 2; i < 12; i++)
                maze.EatCell(i, 0);
            Assert.AreEqual(100, _controller.GetScore());
        }

        [Test]
        public void Eating_Ghost_1_InSuccession_Grants_200_Points_For_200_Total()
        {
            var maze = new Maze("Scene/Maze/maze_", "100,101,102,103,104,44", 3, 45);
            _controller = new MockGameController(maze);
            var player = _controller.GetPlayer();
            for (int i = 1; i < 2; i++)
            {
                _controller.ForceUpdatePosition(player, new Vector2Int(i,0));
                _controller.GetGhosts()[i-1].ActScared(10);
                _controller.ForceOnMoved(player);
            }
            Assert.AreEqual(200, _controller.GetScore());
        }

        [Test]
        public void Eating_Ghost_2_InSuccession_Grants_400_Points_For_600_Total()
        {
            var maze = new Maze("Scene/Maze/maze_", "100,101,102,103,104,44", 3, 45);
            _controller = new MockGameController(maze);
            var player = _controller.GetPlayer();
            for (int i = 1; i < 3; i++)
            {
                _controller.ForceUpdatePosition(player, new Vector2Int(i,0));
                _controller.GetGhosts()[i-1].ActScared(10);
                _controller.ForceOnMoved(player);
            }
            Assert.AreEqual(600, _controller.GetScore());
        }

        [Test]
        public void Eating_Ghost_3_InSuccession_Grants_800_Points_For_1400_Total()
        {
            var maze = new Maze("Scene/Maze/maze_", "100,101,102,103,104,44", 3, 45);
            _controller = new MockGameController(maze);
            var player = _controller.GetPlayer();
            for (int i = 1; i < 4; i++)
            {
                _controller.ForceUpdatePosition(player, new Vector2Int(i,0));
                _controller.GetGhosts()[i-1].ActScared(10);
                _controller.ForceOnMoved(player);
            }
            Assert.AreEqual(1400, _controller.GetScore());
        }

        [Test]
        public void Eating_Ghost_4_InSuccession_Grants_1600_Points_For_3000_Total()
        {
            var maze = new Maze("Scene/Maze/maze_", "100,101,102,103,104,44", 3, 45);
            _controller = new MockGameController(maze);
            var player = _controller.GetPlayer();
            for (int i = 1; i < 5; i++)
            {
                _controller.ForceUpdatePosition(player, new Vector2Int(i,0));
                _controller.GetGhosts()[i-1].ActScared(10);
                _controller.ForceOnMoved(player);
            }
            Assert.AreEqual(3000, _controller.GetScore());
        }

        [Test]
        public void Eating_Ghosts_Not_InSuccession_Grant_200_Points_Each_For_800_Total()
        {
            var maze = new Maze("Scene/Maze/maze_", "100,101,102,103,104,44", 3, 45);
            _controller = new MockGameController(maze);
            var player = _controller.GetPlayer();
            for (int i = 1; i < 5; i++)
            {
                _controller.SetGhostsInSuccession(0);
                _controller.ForceUpdatePosition(player, new Vector2Int(i,0));
                _controller.GetGhosts()[i-1].ActScared(10);
                _controller.ForceOnMoved(player);
            }
            Assert.AreEqual(800, _controller.GetScore());
        }

        [Test]
        public void Eating_PowerPellet_Grants_50_Points()
        {
            var maze = new Maze("Scene/Maze/maze_", "100,101,47,44", 3, 45);
            _controller = new MockGameController(maze);
            maze.EatCell(2, 0);
            Assert.AreEqual(50, _controller.GetScore());
        }

        [Test]
        public void Getting_10000_Score_Grants_A_Life()
        {
            var maze = new Maze("Scene/Maze/maze_", "100,101,44,23", 3, 45);
            _controller = new MockGameController(maze);
            _controller.SetScore(9990);
            maze.EatCell(2, 0);
            Assert.AreEqual(10000, _controller.GetScore());
            Assert.AreEqual(4, _controller.GetLives());
        }

        [Test]
        public void Getting_100000_Score_Grants_A_Life()
        {
            var maze = new Maze("Scene/Maze/maze_", "100,101,44,44", 3, 45);
            _controller = new MockGameController(maze);
            _controller.SetScore(9990);
            maze.EatCell(2, 0);
            _controller.SetScore(99990);
            maze.EatCell(3, 0);
            Assert.AreEqual(100000, _controller.GetScore());
            Assert.AreEqual(5, _controller.GetLives());
        }

        [TearDown]
        public void TearDown()
        {
            _controller?.Dispose();
        }
    }
}