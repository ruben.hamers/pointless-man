using System.Linq;
using HamerSoft.PointlessMan.Core.Game;
using NUnit.Framework;

namespace PointlessMan.Tests.GameControllerTests
{
    public class TeleportingTests : AbstractGameControllerTests
    {
        private MockGameController _controller;

        [Test]
        public void WhenGhost_Teleports_HisSpeed_Is_SlowedDown()
        {
            var maze = new Maze("Scene/Maze/maze_", "44,44,100,0,101", 3, 45);
            _controller = new MockGameController(maze);
            var ghost = _controller.GetGhosts().First();
            var normalSpeed = _controller.GetGhostSpeed(ghost);
            _controller.ForceOnTeleported(ghost);
            Assert.Greater(normalSpeed, _controller.GetGhostSpeed(ghost));
        }

        [TearDown]
        public void TearDown()
        {
            _controller?.Dispose();
            _controller = null;
        }
    }
}