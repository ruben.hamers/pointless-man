using System.Text;
using HamerSoft.PointlessMan.Core.Game;
using NUnit.Framework;

namespace PointlessMan.Tests.MazeTests
{
    public class EatingTests
    {
        private Maze _maze;

        [SetUp]
        public void Setup()
        {
            var sb = new StringBuilder();
            sb.AppendLine("44,44,0");
            _maze = new Maze("Scene/Maze/maze_", sb.ToString(), 1, 45);
        }

        [Test]
        public void WhenThereIsSomethingToEat_TheMazeIsNotWon()
        {
            Assert.False(_maze.GetCellAtGridPos(0, 0).IsEaten);
            Assert.False(_maze.Won());
        }

        [Test]
        public void NonTraversableCells_AreNotEatable()
        {
            Assert.False(_maze.GetCellAtGridPos(2, 0).IsTraversable);
            Assert.True(_maze.GetCellAtGridPos(2, 0).IsEaten);
        }

        [Test]
        public void WhenCellIsEaten_ItIsNoLongerEatable()
        {
            Assert.False(_maze.GetCellAtGridPos(0, 0).IsEaten);
            _maze.EatCell(0, 0);
            Assert.True(_maze.GetCellAtGridPos(0, 0).IsEaten);
        }

        [Test]
        public void WhenAllEatableCellsAreEaten_TheMazeIsWon()
        {
            _maze.EatCell(0, 0);
            _maze.EatCell(1, 0);
            Assert.True(_maze.Won());
        }
    }
}