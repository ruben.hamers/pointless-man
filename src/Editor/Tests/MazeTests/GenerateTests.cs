using System.Text;
using HamerSoft.PointlessMan.Core.Game;
using NUnit.Framework;

namespace PointlessMan.Tests.MazeTests
{
    public class MazeGenerateTests
    {
        [Test]
        public void WhenMazeIsGenerated_ThereIsAlsoAStringRepresentation()
        {
            var maze = GenerateSimpleMaze();
            Assert.NotNull(maze.GetStringRepresentation());
            Assert.AreEqual(2, maze.GetStringRepresentation().Length);
            Assert.AreEqual(28, maze.GetStringRepresentation()[0].Length);
            Assert.AreEqual(28, maze.GetStringRepresentation()[1].Length);
        }

        [Test]
        public void MazeStringRepresentation_ShowsWhichCellsAreTraversable()
        {
            var sb = new StringBuilder();
            for (int i = 0; i < 47; i++)
                sb.Append($"{i.ToString()},");
            var maze = new Maze("Scene/Maze/maze_", sb.ToString(), 1, 45);
            var stringRep = maze.GetStringRepresentation()[0];
            for (int i = 0; i < stringRep.Length; i++)
                Assert.AreEqual(i <= 43 ? "0" : "1", stringRep[i]);
        }

        [Test]
        public void TraversableCellsWillSpawnAPellet()
        {
            var maze = GenerateSimpleMaze();
            Assert.False(maze.GetCellAtGridPos(1, 1).IsEaten);
        }

        [Test]
        public void MazeCanContainTraversableTiles_ThatDoNotSpawnPellets()
        {
            var maze = new Maze("Scene/Maze/maze_", "101,48,49,44", 1, 45);
            Assert.True(maze.GetCellAtGridPos(1, 0).IsEaten);
            Assert.True(maze.GetCellAtGridPos(2, 0).IsEaten);
            Assert.False(maze.GetCellAtGridPos(3, 0).IsEaten);
        }

        private static Maze GenerateSimpleMaze()
        {
            var sb = new StringBuilder();
            sb.AppendLine("1,10,10,10,10,10,10,10,10,10,10,10,10,0,1,10,10,10,10,10,10,10,10,10,10,10,10,0");
            sb.AppendLine("2,44,44,44,44,44,44,44,44,44,44,44,44,2,2,44,44,44,44,44,44,44,44,44,44,44,44,2");
            var maze = new Maze("Scene/Maze/maze_", sb.ToString(), 1, 45);
            return maze;
        }
    }
}