using System.Collections.Generic;
using System.Text;
using HamerSoft.PointlessMan.Core.Game;
using HamerSoft.PointlessMan.Core.Game.Player;
using NUnit.Framework;
using UnityEngine;

namespace PointlessMan.Tests.MazeTests
{
    public class MazeReplacementTests
    {
        [Test]
        public void TestPlayerReplacement()
        {
            var maze = GenerateMaze();
            var player = new Player("Scene/Maze/player_", new Rect(), maze);
            Assert.AreEqual(new Vector2(16, 8), player.Position);
        }

        [Test]
        public void TestGhostReplacements()
        {
            var maze = GenerateMaze();
            List<Ghost> ghosts = new List<Ghost>();
            Rect? ghostPos;
            while ((ghostPos = maze.ReplaceInitialGhostPosition(44, out var initialPos)) != null)
                ghosts.Add(new Ghost("Scene/Maze/ghost_", ghostPos.Value, maze, initialPos));
            Assert.AreEqual(new Vector2(0, 16), ghosts[0].Position);
            Assert.AreEqual(new Vector2(8, 16), ghosts[1].Position);
            Assert.AreEqual(new Vector2(16, 16), ghosts[2].Position);
            Assert.AreEqual(new Vector2(24, 16), ghosts[3].Position);
        }

        private static Maze GenerateMaze()
        {
            var sb = new StringBuilder();
            sb.AppendLine("0,0,0,0");
            sb.AppendLine("0,0,100,0");
            sb.AppendLine("101,101,500,999");
            var maze = new Maze("Scene/Maze/maze_", sb.ToString(), 1, 45);
            return maze;
        }
    }
}