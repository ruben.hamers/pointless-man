using System.Reflection;
using HamerSoft.PointlessMan.Core;
using NUnit.Framework;
using UnityEngine;

namespace PointlessMan.Tests.MovableImageTests
{
    public class SpriteSwapTests
    {
        private class MovableMock : MovableImage
        {
            public MovableMock(string path, Rect rect) : base(path, rect)
            {
            }

            public void SwapSprite(int index)
            {
                typeof(MovableImage)
                    .GetMethod("SwapSprite", BindingFlags.Instance | BindingFlags.NonPublic)
                    .Invoke(this, new object[] {index});
            }

            public int CurrentImage()
            {
                return CurrentTexture;
            }
        }

        [Test]
        public void SwapSprite_Switches_TheCurrentImage()
        {
            var mock = new MovableMock("Scene/Player/", new Rect());
            Assert.AreEqual(0, mock.CurrentImage());
            mock.SwapSprite(0);
            Assert.AreEqual(1, mock.CurrentImage());
        }

        [Test]
        public void SwapSprite_SwitchesBack_ToOriginalIndexWhenInvokedTwice()
        {
            var mock = new MovableMock("Scene/Player/", new Rect());
            Assert.AreEqual(0, mock.CurrentImage());
            mock.SwapSprite(0);
            mock.SwapSprite(0);
            Assert.AreEqual(0, mock.CurrentImage());
        }
    }
}