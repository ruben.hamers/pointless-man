using System.Collections.Generic;
using System.Linq;
using System.Text;
using HamerSoft.PointlessMan.Core.Game;
using HamerSoft.PointlessMan.Core.Game.Pathfinding;
using HamerSoft.PointlessMan.Core.Game.Player;
using NUnit.Framework;
using UnityEngine;

namespace PointlessMan.Tests.PathFinding
{
    public abstract class AbstractPathfinderTests
    {
        protected Maze Maze;
        protected IPathfinder Pathfinder;

        [SetUp]
        public abstract void Setup();

        protected List<Vector2Int> FindPath(string layout)
        {
            Maze = new Maze("Scene/Maze/maze_", layout, 3, 45);
            var player = new Player("Scene/Player/", new Rect(), Maze);
            var ghost = new Ghost("Scene/Player/", Maze.ReplaceInitialGhostPosition(44, out Vector2Int gridpos).Value,
                Maze, gridpos);
            return Pathfinder.FindPath(Maze, gridpos, Maze.CurrentPlayerPos)?.ToList();
        }

        protected static StringBuilder GetSimpleMaze()
        {
            var sb = new StringBuilder();
            sb.AppendLine("44,44,44,44,100,0");
            sb.AppendLine("44,44,44,44,44,0");
            sb.AppendLine("44,44,44,44,44,0");
            sb.AppendLine("101,44,44,44,44,0");
            sb.AppendLine("0,0,0,0,0,0");
            return sb;
        }

        [TearDown]
        public void TearDown()
        {
            Maze = null;
            Pathfinder = null;
        }
    }
}