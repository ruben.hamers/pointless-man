using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using PointlessMan.Core.Game.PathfindingAlgorithms;
using UnityEngine;

namespace PointlessMan.Tests.PathFinding
{
    public class BlockingTests : AbstractPathfinderTests
    {
        [SetUp]
        public override void Setup()
        {
            Pathfinder = new Blocking(2);
        }

        [Test]
        public void WhenPathIsFound_TheEndPos_IsNotEqualTo_ThePlayerPos()
        {
            var sb = GetSimpleMaze();
            var path = FindPath(sb.ToString());
            Assert.AreNotEqual(path.Last(), Maze.CurrentPlayerPos);
        }

        [Test]
        public void WhenPathIsFound_ItHasAnOffSetOf_2_Frontiers()
        {
            var sb = GetSimpleMaze();
            var path = FindPath(sb.ToString());
            List<Vector2Int> excludedPositions = new List<Vector2Int>
            {
                new Vector2Int(2, 0),
                new Vector2Int(3, 1),
                new Vector2Int(4, 2),
            };
            Assert.True(excludedPositions.Any(p => p == path.Last()));
        }

        [Test]
        public void WhenOffsetIs_Partly_Obstructed_The_Offset_WillBe_Less()
        {
            var sb = new StringBuilder();

            sb.AppendLine("44,44,0,44,100,0");
            sb.AppendLine("44,44,44,44,44,0");
            sb.AppendLine("44,44,44,44,0");
            sb.AppendLine("101,44,44,44,44,0");
            sb.AppendLine("0,0,0,0,0,0");
            var path = FindPath(sb.ToString());
            List<Vector2Int> excludedPositions = new List<Vector2Int>
            {
                new Vector2Int(3, 1),
            };
            Assert.True(excludedPositions.Any(p => p == path.Last()));
        }

        [Test]
        public void WhenOffsetIs_Obstructed_There_Cannot_Be_A_Path()
        {
            var sb = new StringBuilder();
            sb.AppendLine("44,44,0,44,100,0");
            sb.AppendLine("44,44,44,0,44,0");
            sb.AppendLine("44,44,44,44,0");
            sb.AppendLine("101,44,44,44,44,0");
            sb.AppendLine("0,0,0,0,0");
            var path = FindPath(sb.ToString());
            Assert.Null(path);
        }
    }
}