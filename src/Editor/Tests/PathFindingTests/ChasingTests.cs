using System.Collections.Generic;
using System.Linq;
using System.Text;
using HamerSoft.PointlessMan.Core.Game;
using HamerSoft.PointlessMan.Core.Game.Player;
using NUnit.Framework;
using PointlessMan.Core.Game.PathfindingAlgorithms;
using UnityEngine;

namespace PointlessMan.Tests.PathFinding
{
    public class ChasingTests : AbstractPathfinderTests
    {
        [SetUp]
        public override void Setup()
        {
            Pathfinder = new Chasing();
        }

        [Test]
        public void GhostCanFindPathWhenNextToPlayer()
        {
            var maze = new Maze("Scene/Maze/maze_", "101,100", 3, 45);
            var player = new Player("Scene/Player/", new Rect(), maze);
            var pathFinder = new Chasing();
            var path = pathFinder.FindPath(maze, new Vector2Int(0, 0), maze.CurrentPlayerPos).ToList();
            Assert.True(path.Count == 2);
            Assert.True(path[1] == new Vector2Int(1, 0));
        }

        [Test]
        public void GhostCanFindPathWhenEmptySpaceInBetween()
        {
            var path = FindPath("101,44,100,0");
            Assert.True(path.Count == 3);
            Assert.True(path[1] == new Vector2Int(1, 0));
            Assert.True(path[2] == new Vector2Int(2, 0));
        }

        [Test]
        public void GhostCannotFindPathWhenPathIsObstructed()
        {
            var path = FindPath("101,0,100,0");
            Assert.IsNull(path);
        }

        [Test]
        public void GhostCanFindPathAroundStuff()
        {
            var sb = new StringBuilder();
            sb.AppendLine("44,44,44,44,100,0");
            sb.AppendLine("44,0,0,0,0");
            sb.AppendLine("44,44,44,44,101,0");
            sb.AppendLine("0,0,0,0,0");
            var path = FindPath(sb.ToString());
            Assert.True(path.Count == 11);
            Assert.True(path[0] == new Vector2Int(4, 2));
            Assert.True(path[1] == new Vector2Int(3, 2));
            Assert.True(path[2] == new Vector2Int(2, 2));
            Assert.True(path[3] == new Vector2Int(1, 2));
            Assert.True(path[4] == new Vector2Int(0, 2));
            Assert.True(path[5] == new Vector2Int(0, 1));
            Assert.True(path[6] == new Vector2Int(0, 0));
            Assert.True(path[7] == new Vector2Int(1, 0));
            Assert.True(path[8] == new Vector2Int(2, 0));
            Assert.True(path[9] == new Vector2Int(3, 0));
            Assert.True(path[10] == new Vector2Int(4, 0));
        }
    }
}