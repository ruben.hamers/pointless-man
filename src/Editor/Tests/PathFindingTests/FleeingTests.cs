using NUnit.Framework;
using PointlessMan.Core.Game.PathfindingAlgorithms;

namespace PointlessMan.Tests.PathFinding
{
    public class FleeingTests : AbstractPathfinderTests
    {
        [SetUp]
        public override void Setup()
        {
            Pathfinder = new Fleeing(2);
        }

        [Test]
        public void WhenFleeing_The_PlayerPosition_Is_Not_In_The_Path()
        {
            var sb = GetSimpleMaze();
            var path = FindPath(sb.ToString());
            Assert.False(path.Contains(Maze.CurrentPlayerPos));
        }
    }
}