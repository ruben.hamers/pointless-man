using System.Linq;
using NUnit.Framework;
using PointlessMan.Core.Game.PathfindingAlgorithms;

namespace PointlessMan.Tests.PathFinding
{
    public class ScaredTests: AbstractPathfinderTests
    {
        [SetUp]
        public override void Setup()
        {
            Pathfinder = new Scared(2);
        }

        [Test]
        public void WhenFleeing_The_PlayerPosition_Is_Not_TheLast()
        {
            var sb = GetSimpleMaze();
            var path = FindPath(sb.ToString());
            Assert.False(path.Last() == Maze.CurrentPlayerPos);
        }
    }
}