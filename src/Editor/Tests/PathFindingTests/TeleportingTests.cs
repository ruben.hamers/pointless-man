using System.Text;
using NUnit.Framework;
using PointlessMan.Core.Game.PathfindingAlgorithms;
using UnityEngine;

namespace PointlessMan.Tests.PathFinding
{
    public class TeleportingTests : AbstractPathfinderTests
    {
        [SetUp]
        public override void Setup()
        {
            Pathfinder = new Chasing();
        }

        [Test]
        public void Ghost_Can_Teleport_From_LeftToRight()
        {
            var path = FindPath("100,0,101");
            Assert.True(path.Count == 2);
            Assert.True(path[0] == new Vector2Int(2, 0));
            Assert.True(path[1] == new Vector2Int(0, 0));
        }

        [Test]
        public void Ghost_Can_Teleport_From_RightToLeft()
        {
            var path = FindPath("101,0,100");
            Assert.True(path.Count == 2);
            Assert.True(path[0] == new Vector2Int(0, 0));
            Assert.True(path[1] == new Vector2Int(2, 0));
        }

        [Test]
        public void Ghost_Can_Teleport_From_TopToBottom()
        {
            var sb = new StringBuilder();
            sb.AppendLine("101");
            sb.AppendLine("0");
            sb.AppendLine("100");
            var path = FindPath(sb.ToString());
            Assert.True(path.Count == 2);
            Assert.True(path[0] == new Vector2Int(0, 0));
            Assert.True(path[1] == new Vector2Int(0, 2));
        }

        [Test]
        public void Ghost_Can_Teleport_From_BottomToTop()
        {
            var sb = new StringBuilder();
            sb.AppendLine("100");
            sb.AppendLine("0");
            sb.AppendLine("101");
            var path = FindPath(sb.ToString());
            Assert.True(path.Count == 2);
            Assert.True(path[0] == new Vector2Int(0, 2));
            Assert.True(path[1] == new Vector2Int(0, 0));
        }

        [Test]
        public void Ghost_CanNot_Teleport_WhenOpposite_OfYAxis_IsObstructed()
        {
            var sb = new StringBuilder();
            sb.AppendLine("100");
            sb.AppendLine("0");
            sb.AppendLine("101");
            sb.AppendLine("0");
            var path = FindPath(sb.ToString());
            Assert.Null(path);
        }

        [Test]
        public void Ghost_CanNot_Teleport_WhenOpposite_OfXAxis_IsObstructed()
        {
            var path = FindPath("101,0,100,0");
            Assert.Null(path);
        }

        [Test]
        public void GhostCanFindAPath_ThroughObstructed_Maze_By_Teleports()
        {
            var sb = new StringBuilder();
            sb.AppendLine("0,0,0,0,0,0"); //0
            sb.AppendLine("44,44,0,0,100,44"); //1
            sb.AppendLine("0,44,0,0,0,0"); // 2
            sb.AppendLine("44,44,0,0,44,44"); //3
            sb.AppendLine("0,0,0,101,44,0"); //4
            sb.AppendLine("0,0,0,0,0,0");
            var path = FindPath(sb.ToString());
            Assert.AreEqual(new Vector2Int(3, 4), path[0]);
            Assert.AreEqual(new Vector2Int(4, 4), path[1]);
            Assert.AreEqual(new Vector2Int(4, 3), path[2]);
            Assert.AreEqual(new Vector2Int(5, 3), path[3]);
            Assert.AreEqual(new Vector2Int(0, 3), path[4]);
            Assert.AreEqual(new Vector2Int(1, 3), path[5]);
            Assert.AreEqual(new Vector2Int(1, 2), path[6]);
            Assert.AreEqual(new Vector2Int(1, 1), path[7]);
            Assert.AreEqual(new Vector2Int(0, 1), path[8]);
            Assert.AreEqual(new Vector2Int(5, 1), path[9]);
            Assert.AreEqual(new Vector2Int(4, 1), path[10]);
        }
    }
}