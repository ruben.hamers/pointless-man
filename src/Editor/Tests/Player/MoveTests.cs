﻿using System;
using System.Reflection;
using System.Text;
using HamerSoft.PointlessMan.Core.Game;
using HamerSoft.PointlessMan.Core.Game.Player;
using NUnit.Framework;
using UnityEngine;

namespace PointlessMan.Tests.PlayerTests
{
    public class PlayerTests
    {
        protected class MockPlayer : Player
        {
            private delegate bool canMove();

            private canMove Left, Right, Up, Down;

            public MockPlayer(string path, Rect rect, Maze maze) : base(path, rect, maze)
            {
                Left = GetCanMove("CanMoveLeft");
                Right = GetCanMove("CanMoveRight");
                Up = GetCanMove("CanMoveUp");
                Down = GetCanMove("CanMoveDown");
            }

            private canMove GetCanMove(string name)
            {
                return (canMove) Delegate.CreateDelegate(
                    typeof(canMove),
                    this,
                    typeof(Player).GetMethod(
                        name,
                        BindingFlags.NonPublic | BindingFlags.Instance));
            }

            public bool CanMoveLeft()
            {
                return Left();
            }

            public bool CanMoveRight()
            {
                return Right();
            }

            public bool CanMoveUp()
            {
                return Up();
            }

            public bool CanMoveDown()
            {
                return Down();
            }
        }
    }

    public class MoveTests : PlayerTests
    {
        private Maze _maze;
        private MockPlayer _player;


        private void GenerateMaze(Vector2 playerPos)
        {
            _maze = CreateMaze(playerPos);
            _player = new MockPlayer("Scene/Player/", new Rect(), _maze);
            _player.SetMoveSpeed(5f);
        }

        [Test]
        public void PlayerCannotMoveLeft_WhenTheTargetIsNotTraversable()
        {
            GenerateMaze(new Vector2(3, 3));
            Assert.False(_player.CanMoveLeft());
        }

        [Test]
        public void PlayerCannotMoveRight_WhenTheTargetIsNotTraversable()
        {
            GenerateMaze(new Vector2(3, 3));
            Assert.False(_player.CanMoveRight());
        }

        [Test]
        public void PlayerCannotMoveUp_WhenTheTargetIsNotTraversable()
        {
            GenerateMaze(new Vector2(3, 1));
            Assert.False(_player.CanMoveUp());
        }

        [Test]
        public void PlayerCannotMoveDown_WhenTheTargetIsNotTraversable()
        {
            GenerateMaze(new Vector2(2, 1));
            Assert.False(_player.CanMoveDown());
        }

        [Test]
        public void PlayerCanMoveUp_WhenTheTargetIsTraversable()
        {
            GenerateMaze(new Vector2(3, 3));
            Assert.True(_player.CanMoveUp());
        }

        [Test]
        public void PlayerCanMoveDown_WhenTheTargetIsTraversable()
        {
            GenerateMaze(new Vector2(3, 3));
            Assert.True(_player.CanMoveDown());
        }

        [Test]
        public void PlayerCanMoveLeft_WhenTheTargetIsTraversable()
        {
            GenerateMaze(new Vector2(3, 1));
            Assert.True(_player.CanMoveLeft());
        }

        [Test]
        public void PlayerCanMoveRight_WhenTheTargetIsTraversable()
        {
            GenerateMaze(new Vector2(2, 1));
            Assert.True(_player.CanMoveRight());
        }

        private static Maze CreateMaze(Vector2 playerPos)
        {
            string[] line0 = new[] {"0", "0", "0", "0", "0", "0", "0"};
            string[] line1 = new[] {"0", "44", "44", "44", "44", "44", "0"};
            string[] line2 = new[] {"0", "44", "0", "44", "0", "44", "0"};
            string[] line3 = new[] {"0", "44", "0", "44", "0", "44", "0"};
            string[] line4 = new[] {"0", "44", "44", "44", "44", "44", "0"};
            string[] line5 = new[] {"0", "0", "0", "0", "0", "0", "0"};
            string[][] grid = new[] {line0, line1, line2, line3, line4, line5};
            grid[(int) playerPos.y][(int) playerPos.x] = "100";

            var sb = new StringBuilder();
            foreach (string[] strings in grid)
                sb.AppendLine(string.Join(",", strings));
            var maze = new Maze("Scene/Maze/maze_", sb.ToString(), 3,45);
            return maze;
        }
    }
}